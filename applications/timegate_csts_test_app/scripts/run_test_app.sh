# manually load target bitstream
ocpihdl -d pl:0 load ../../artifacts/ocpi.osp.epiq_solutions.timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.hdl.0.z3u.bitz

#sleep 30

# reset time_server time_now register
ocpihdl -d pl:0 set time_server time_now "0"

# run application
ocpirun -v -x -t 5 ./timegate_csts_test_app.xml

# print out time_server properties
ocpihdl -d pl:0 get -v 1

# reset bitstream
ocpihdl -d pl:0 load ../../artifacts/ocpi.osp.epiq_solutions.timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.hdl.0.z3u.bitz
