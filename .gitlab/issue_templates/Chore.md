### Task
(More or less a catch-all if one of the other issue templates don't fit.
Usually a chore is something that is not fixing a bug or implementing a
feature.)

### Acceptance criteria
(What would it take for you to feel this issue can be closed?)
