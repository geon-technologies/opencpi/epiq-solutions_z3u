.. Guide for developing the Epiq Solutions Matchstiq Z3u OSP

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _dev-Guide for developing the Epiq Solutions Matchstiq Z3u OSP:

Guide for developing the Epiq Solutions Matchstiq Z3u OSP
=========================================================

.. _dev-Introduction-label:

Introduction
------------

This document serves as a guide for developing an OpenCPI System support Project (OSP) for the Epiq
Solutions Matchstiq Z3u SDR, herein referred to as ``Z3u``. The Z3u is a field-ready,
complete SDR platform designed to deliver a fully integrated RF transceiver and signal processing
solution in the smallest possible form factor. The Z3u implements a Xilinx Zynq UltraScale+ MPSoC
and an AD9361 that has a tunable range between 70 MHz to 6 GHz and RF bandwidth of 200 kHz up to 56
MHz.

The OSP supports the following minimum requirement:

#. Control Plane via a single Master AXI High-Performance Memory Mapped port of the the MPSoC

#. Data Plane via four Slave Streaming-AXI High-Performance ports of the MPSoC

#. Platform Configurations: base

#. Several device/proxy workers to interface with devices that are external to the UltraScale+
   MPSoC

Since the Xilinx Zynq UltraScale+ MPSoC on the Z3u is similar to the zcu104 platform, the zcu104
OSP is referenced heavily in this guide. In fact, the zcu104's OSP is examined in the following
areas and serves as the benchmark for defining the requirements for the Z3u's OSP:

#. Understand the output artifacts of performing an OpenCPI *install* and *deploy* of the zcu104
   with xilinx18_3_aarch64

#. Review of the HDL primitive and HDL platform worker source code for implementing the zcu104

#. Benchmarking:

   #. Control Plane (CP) ONLY verification on the zcu104

      #. CP reference application, i.e. ``pattern_capture``

   #. Data Plane (DP) verification of the following on the zcu104

      #. DP reference application, i.e. ``testbias``

      #. FSK ``filerw``

      #. All viable Component Unit Tests to establish their PASS/FAIL benchmarks

   #. Support for the Analog Devices transceiver (AD9361)

      #. Verification is performed with an fmcomms2 daughter card installed in the FMC_LPC slot of
         the zcu104 and successful execution of the fsk_modem_app application


.. _dev-Deliverables-label:

Deliverables
^^^^^^^^^^^^

#. An OpenCPI project named, ``ocpi.osp.epiq_solutions``

#. One OpenCPI Board Support Package (OSP)

   HDL Platform named ``z3u``, with an RCC platform dependency on ``xilinx18_3_aarch64``

#. Supports the HDL Control Plane, HDL Data Plane, interfaces of the AD9361 and various other
   devices in a manner that is comparable with the vendor provided SDR

#. Documentation

   #. OpenCPI Z3u Getting Started Guide

   #. Guide for developing the Epiq Solutions Matchstiq Z3u OSP (This guide)

   #. Guide for developing Device / Proxy Workers for i2c an bus

   #. Datasheets for each application, Device Worker, and Device Proxy

.. _dev-Prerequisites-label:

Prerequisites
^^^^^^^^^^^^^

#. *Knowledge*

   #. A working knowledge of the OpenCPI framework is a must. Its documentation is located
      at https://opencpi.gitlab.io/releases/latest/. Close attention must be made to the following:

      - `OpenCPI Platform Development Guide <https://opencpi.gitlab.io/releases/v2.2.1/docs/OpenCPI_Platform_Development_Guide.pdf>`_

      - `OpenCPI Component Development Guide (Component Unit Test) <https://opencpi.gitlab.io/releases/v2.2.1/docs/OpenCPI_Component_Development_Guide.pdf>`_

      - Getting Started Guide for the ZCU104

      - Getting Started Guide for the FSK applications

      - Support documentation for the fmcomms2/3 daughtercard

   #. Epiq Solutions, Matchstiq Z3u

      .. note::

        The following documentation must be requested directly from Epiq Solutions

      ..

      #. Documents:

         #. Matchstiq Z3u Getting Started Guide v1.0

         #. Matchstiq Z3u Hardware User Manual v1.3

         #. Matchstiq Z3u Firmware Developer's Guide v1.0.0

      #. Vendor reference design package

#. *Equipment*

   +------------------+--------------------------------------------------------------------------+
   | **Item**         | Matchstiq Z3u SDR                                                        |
   +------------------+--------------------------------------------------------------------------+
   | **Desciption**   | Software Defined Radio that hosts a Zynq UltraScale+ MPSoC and a AD9361  |
   |                  | transceiver that supports 1Tx/1Rx or 2Rx modes                           |
   +------------------+--------------------------------------------------------------------------+
   | **Vendor**       | Epiq Solutions                                                           |
   +------------------+--------------------------------------------------------------------------+
   | **Version**      | libsidekiq    : v4.15.1                                                  |
   |                  | FPGA Firmware : v3.14.1 (0x08fefa52)                                     |
   |                  | Device s/n    : 9X1N (rev C)                                             |
   +------------------+--------------------------------------------------------------------------+
   | **P/N**          | X                                                                        |
   +------------------+--------------------------------------------------------------------------+
   | **Cost**         | X                                                                        |
   +------------------+--------------------------------------------------------------------------+

#. *Tools*

   #. Xilinx tools:

      #. Vivado/SDK 2018.3 (Recommend downloading the ``Unified Installer``)

      #. PetaLinux: v2018.3

   #. OpenCPI FOSS framework, https://gitlab.com/opencpi/opencpi.git

      - Release v2.2.0

      .. note::

         Bug fixes and enhancements are detailed in this guide

      ..


.. _dev-Preview-of-the-concluding-directory-tree-label:

Preview of the concluding directory tree
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This guide requires the creation of many directories. A summary of those directories are provided
below:

**Clone of OpenCPI FOSS framework**

   ::

      /home/user/opencpi

   ..

**Clone of OpenCPI's Ettus e3xx OSP**

   ::

      /home/user/ocpi.osp.e3xx

   ..

**OpenCPI project repo directory for Z3u**

   ::

      /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions

   ..

**Vendor's reference design(s)**

   ::

      /home/user/vendor_reference_design_package
      /home/user/vendor_reference_design_package_gui
      /home/user/vendor_reference_design_package_gui_saved

   ..

**Petalinux directories**

   ::

      /tmp/z3u/petalinux_bsp

   ..

**Processing-System Vivado project directories**

   ::

      /home/user/z3u_ocpi_cp
      /home/user/z3u_ocpi_dp

   ..

.. _dev-Review-Vendor's-Reference-Design-Package-label:

Review Vendor's Reference Design Package
----------------------------------------

**GOAL:**

- Allow the user to become familiar with the target board and its reference design package. By
  working through the reference design package, the user becomes comfortable with various tools,
  modifying source code, understanding the build flow (Vivado, PetaLinux), creating a microSD card,
  loading the FPGA and running an application. The build flow steps captured during the review are
  heavily relied upon throughout much of this guide and its lessons-learned are used during the
  integration with OpenCPI.

   - Review its documentation and work though the reference designs that target the processor
     and/or the FPGA.

   - (OPTIONAL) Verify that the reference design can be modified and rebuilt so that a value of a
     register is set at build-time and readback during run-time. Then build the bitstream,
     create an image via PetaLinux, install on the Z3u and readback the value from the
     custom register.

**IMPLEMENTATION:**

#. Install reference design package:

   #. Create a directory named ``/home/user/vendor_reference_design_package``

      ``$ mkdir /home/user/vendor_reference_design_package``

   #. Download the reference design package labeled ``matchstiq_z3u_pdk_v3_15_1.tar.gz`` and place
      it in the newly created directory.

      .. note::

         **The reference design contains the FPGA constraints file(s) used in the
         development of the OSP**

      ..

      Untar and unzip the PDK project:

         ``$ tar -xvzf matchstiq_z3u_pdk_v3_15_1.tar.gz``

         ``$ unzip hdl-b4335123a3d21dadcf3d04ad09411d1d2954ebd7.zip``


#. Place the Z3u (based on Sidekiq Z2p) Vivado board files into your Vivado installation directory:

   ``$ tar -xvzf board_files.tar.gz``

   ``$ cd /home/user/vendor_reference_design_package/board_files``

   ``$ cp -rf sidekiqz2p  /opt/Xilinx/Vivado/2018.3/data/boards/board_files/``

   ``$ chmod -R 775 /opt/Xilinx/Vivado/2018.3/data/boards/board_files/sidekiqz2p``

   ``$ cd /home/user/vendor_reference_design_package``

#. Build the project

   ``$ source /opt/Xilinx/Vivado/2018.3/settings64.sh``

   ``$ vivado -mode batch -source vivado_build.tcl``

#. Review the vendor provided documentation, such as, but not limited to:

   #. Matchstiq Z3u Getting Started Guide v1.0

   #. Matchstiq Z3u Hardware User Manual v1.3

   #. Matchstiq Z3u Firmware Developer's Guide v1.0.0

#. Per the documentation, setup the development host with appropriate tools and make note of any
   special setup requirements

#. Build the reference design(s): SW and HW

#. Run the various examples application(s)

#. Explore all that has been provided and make note of any special build or run time steps, which
   may be reused in the OpenCPI development flow:

   #. Build the bitstream

   #. Booting the into Z3u

   #. Setting up the microSD card

   #. Loading of the FPGA

   #. Run Application(s)

#. (OPTIONAL) Once you have become familiar with the out-of-the-box reference design, convince
   yourself that you can edit the design by including a register for readback, which supports
   read/write or has a constant value. The details are left to the reader.

.. _dev-Take-inventory-of-the-target-board-label:

Take inventory of the target board
----------------------------------

**BACKGROUND:**

Per the OpenCPI Platform Development Guide, the target board was examined to identify the devices
and interfaces that are required to support a functioning OSP. One of the main focuses of
this phase is to identify the device(s) that will be used as an OpenCPI “container(s)”, which is
where an application or portion of an application can be deployed. The interfaces between all
devices are examined to determine if they are necessary for command/control or to transport payload
data within an OSP. It is necessary to establish which device or interconnect to an external device
serves as the “main processor”, and whose responsibility it is to perform system level
functionality like, command/control, and possibly, transportation of payload data. This serves as
an aid for determining the amount of work necessary to develop an OSP for the target platform.

**GOAL:**

- Identify container(s)

- Identify interface(s)

- These items were sorted into the following categories based on their current support within the
  OpenCPI framework: **Yes, No, Partial.** For items where there is partial or no support, a
  research phase was conducted to ascertain the amount of development that is required to
  understand the level of support that is needed by the target platform. The “level of support”
  varies based on the configuration for a given device versus the requirements of the target
  platform. For example, although the digital interface of the Analog Devices transceiver may
  support LVDS and CMOS electrical standards, the target platform may implement support of only
  the LVDS standard, and therefore it may not be necessary to also enable support for CMOS.
  However, implementing support for CMOS standard could be added at a later time. This also
  highlights any additional requirement(s) for developing support for new tooling or other
  features that must be integrated into the OpenCPI framework to properly target the devices, such
  as FPGA build tools or software cross-compilers. In many cases, this initial investigation into
  the platform can be accomplished from vendor provided documentation, such as,  the User’s guide,
  board schematics and wiring diagrams. In cases where this documentation is insufficient,
  analysis must be performed with access to the board directly. However, it is also possible that
  not enough information can be gathered to successfully develop an OSP, but this should
  be determined upon the completion of the phases described above.

**IMPLEMENTATION:**

+---------------------------------+-------------------------------+------------------------------------+
| Tool/Device/Interface/Function  | Description                   | Framework Support (Yes/No/Partial) |
+=================================+===============================+====================================+
| T: FPGA build tools             | Xilinx Vivado 2018.3          | Yes                                |
+---------------------------------+-------------------------------+------------------------------------+
| D: Xilinx XCZU3EG-1-SBVA484I    | Zynq UltraScale+ MPSoC IC     | Yes                                |
+---------------------------------+-------------------------------+------------------------------------+
| D: Analog Devices AD9361        | RF Transceiver IC             | Yes                                |
+---------------------------------+-------------------------------+------------------------------------+
| D: NXP PCAL6524                 | General Purpose I/O Expander  | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: Atlanta Micro AM3090         | Digitally Tunable Band Pass   | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: Atlanta Micro AM3064         | Digitally Tunable Band Pass   | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: Atlanta Micro AM3102         | Digitally Tunable Band Pass   | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: TDK ICM-20602                | Internal Measurement Unit     | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: Microchip 24AA               | CMOS Serial EEPROM            | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: SiTime SiT5356               | Precision MEMS Super-TCXO     | No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: Texas Instrument TMP103CYFFR | Temperature Sensor            | Similar device supported           |
+---------------------------------+-------------------------------+------------------------------------+
| D: Analog Device ADF4002        | Phase Detector/Freq Synth(PLL)| No                                 |
+---------------------------------+-------------------------------+------------------------------------+
| D: Origin Spider ORG4033        | GPS Receiver                  | No                                 |
+---------------------------------+-------------------------------+------------------------------------+

.. _dev-Update-the-OpenCPI-Framework-label:

Update the OpenCPI Framework
----------------------------

Based on the items identified in the :ref:`dev-Take-inventory-of-the-target-board-label`,
the following is a high-level summary of updates made to the
OpenCPI framework, that are required in support of this guide. The details for each of the updates
is described in other sections.


.. _dev-Enabling-Development-for-New-GPP-Platforms-label:

Enabling Development for New GPP Platforms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When developing an OSP, it is ideal to use the latest developer tool versions for the targeted
devices. **However**, due to the boot scheme implemented by the Z3u, targeting an older SW version
to match the vendor reference design is a risk mitigating first step. So rather than targeting
version 2019.2 of the Xilinx Vivado/Vitis/Petalinux tools suite, tool version 2018.3 is leveraged.

The version (v2.2.0) of OpenCPI used for this development effort does not support this version
of the RCC Platform (2018.3), as such, support must be added. More details are provided in later
sections.


.. _dev-Integrating-FPGA-tool-chain-into-OpenCPI-HDL-build-process-label:

Integrating FPGA tool chain into OpenCPI HDL build process
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The FPGA build tools & versions that are supported by this version of the framework:

  ``XilinxVivado/SDK 2018.3``


.. _dev-Support-for-specific-FPGA-device-label:

Support for specific FPGA device
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

While the target family is supported by the framework, the specific device used by the Z3u is not.
This was determined by reviewing the contents of **/home/user/opencpi/tools/include/hdl
/hdl-targets.mk**. This file is modified in a later section to include support for targeting the
specific device used by the Z3u.

.. _dev-Configure/Boot-Z3u-artifacts-label:

Configure/Boot Z3u artifacts
----------------------------

.. note::

   This section is done prior to any OpenCPI configuration so that once the user can become
   familiar with the boot process and sd-card configuration.

..

**GOAL:**

- Format the microSD card to have ``boot/`` and ``root/`` partitions.

- By default, the ``Z3u`` is configured to load its rootfs (Ubuntu 18.04.5) that is stored on an
  eMMC. For the development of the OSP, the desire is to boot the Z3u into Petalinux. Below are the
  steps that support configuring the ``Z3u`` microSD card artifacts and booting the ``Z3u`` into
  ``Petalinux`` via a microSD.


.. _dev-Format-microSD-card-label:

Format microSD card
^^^^^^^^^^^^^^^^^^^^

**IMPLEMENTATION:**

#. Install the microSD card

#. Be sure to save off any important information on the microSD card

#. ``sudo umount /dev/sda1`` and/or ``sudo umount /dev/sda2``

#. ``sudo fdisk /dev/sda``

#. List the current partition table

   Command (m for help): ``p``

#. Remove all current partitions

   Command (m for help): ``d``

#. Make the following selections to create two partitions

   #. New ``n``, Primary ``p``, Partition number ``1``, First sector [enter] (default),
      Last sector size ``+1G``

   #. New ``n``, Primary ``p``, Partition number ``2``, First sector [enter] (default), Last sector
      size [enter] (default)

#. Write table to disk and exit

   Command (m for help): ``w``

#. Uninstall and reinstall the microSD card / USB drive

#. ``sudo umount /dev/sda1`` and/or ``sudo umount /dev/sda2``

#. ``sudo mkfs.vfat -F 32 -n boot /dev/sda1``

#. ``sudo mkfs.ext4 -L root /dev/sda2``

   ::

      mke2fs 1.42.9 (28-Dec-2013)
      Filesystem label=root
      OS type: Linux
      Block size=4096 (log=2)
      Fragment size=4096 (log=2)
      Stride=0 blocks, Stripe width=0 blocks
      907536 inodes, 3627136 blocks
      181356 blocks (5.00%) reserved for the super user
      First data block=0
      Maximum filesystem blocks=2151677952
      111 block groups
      32768 blocks per group, 32768 fragments per group
      8176 inodes per group
      Superblock backups stored on blocks:
              32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208

      Allocating group tables: done
      Writing inode tables: done
      Creating journal (32768 blocks): done
      Writing superblocks and filesystem accounting information: done

   ..

#. Uninstall and reinstall the microSD card / USB drive

#. Check that both partitions have been made


.. _dev-Configure-Z3u-microSD-card-artifacts-label:

Configure Z3u microSD card artifacts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**IMPLEMENTATION:**

.. note::

   The following installation shell-script (``install_matchstiq_z3u_bsp_20120302.sh``)  must be
   request directly from Epiq Solutions

..

#. Install the ``install_matchstiq_z3u_bsp_20120302.sh``

#. ``$ source /opt/pkg/petalinux/2018.3/settings.sh``

#. ``$ sudo chmod 777 install_matchstiq_z3u_bsp_20210302+.sh``

#. ``$ ./install_matchstiq_z3u_bsp_20210302+.sh``

#. Specify the ``/tmp/z3u`` directory to install to

   ::

      Verifying archive integrity...  100%   All good.
      Uncompressing Matchstiq Z3u BSP  100%
      Specify directory to install to
      /tmp/z3u
      INFO: Create project:
      INFO: Projects:
      INFO:   * petalinux_bsp
      INFO: has been successfully installed to /tmp/z3u/
      INFO: New project successfully created in /tmp/z3u/
      Copying additional Epiq helper scripts

   ..

#. ``$ cd /tmp/z3u/petalinux_bsp/``

#. ``$ petalinux-build``

#. ``$ ./generate_user_package.sh``

   .. warning::

      In the case that the following error occurs

      ::

         FPGA BITSTREAM  = images/linux/system.bit

         Creating temporary staging directory
         Copying build artifacts to staging directory
         Copying FPGA bitstream images/linux/system.bit
         Dumping image contents to staging
         Updating final device tree
         FATAL ERROR: Couldn't open "staging/system_orig.dtb": No such file or directory

      ..

      Update the uboot-tools, by performing ``sudo yum update uboot-tools``, then retry the
      ``./generate_user_package.sh``.

   ..

#. With properly configured microSD card having ``boot/`` and ``root/`` partitions
   (:ref:`dev-Format-microSD-card-label`)

   #. ``$ cp z3u_user.itb /run/media/<user>/boot/image.ub``

   #. ``$ sudo tar xvf images/linux/rootfs.tar.gz -C /run/media/<user>/root/``

   #. ``$ umount /dev/sda1`` and ``umount /dev/sda2``

   #. Remove the microSD card from host


.. _dev-Boot-Z3u-label:

Boot Z3u
^^^^^^^^

#. When power not applied, install microSD into ``Z3u``

#. Attach a serial cable (microUSB to USB-A) from the ``Z3u`` to the ``host``

#. Attach a USB-C cable from the ``Z3u`` to the ``host``. This applies power to the Z3u and begins
   the boot process.

#. Establish a serial connection

   ``$ sudo screen /dev/ttyUSB0 115200``

#. Enter into the U-boot setup. **This may require rebooting the Z3u to catch the U-boot countdown
   and cancel the full boot sequence.**

   ::

      ...
      DRAM:  2 GiB
      EL Level:       EL2
      Chip ID:        zu3eg
      MMC:   sdhci_transfer_data: Error detected in status(0x408000)!
      mmc@ff160000: 0 (SD), mmc@ff170000: 1 (eMMC)
      SF: Detected n25q1024a with page size 256 Bytes, erase size 64 KiB, total 128 MiB
      In:    serial@ff010000
      Out:   serial@ff010000
      Err:   serial@ff010000
      Board: Xilinx ZynqMP
      Hit any key to stop autoboot:  0
      ZynqMP>

#. Within the U-boot editor, set environment variables to temporarily boot from the microSD card:

   ::

      ZynqMP> setenv force_sdboot 1
      ZynqMP> setenv bootargs ${bootargs} root=/dev/mmcblk0p2 rootwait rw
      ZynqMP> boot

   ..

   .. note::

      It is possible to save these U-boot settings so that it presists upon reboot or power
      cycling, but that is left to the reader.

   ..


#. Verify that the following login is observed (usr/pw = root/root)::

      ...
      PetaLinux 2018.3 z3u /dev/ttyPS0

      z3u login: root
      Password:
      root@z3u:~# uname -a
      Linux z3u 4.14.0-xilinx-v2018.3 #1 SMP Tue Dec 7 19:58:12 UTC 2021 aarch64 GNU/Linux

.. warning::

   If you are presented with the error in the ``codeblock`` after entering the z3u login
   credentials, remove the ``/home/root/.profile``, reboot the z3u and login.


   ::

      z3u login: root
      Password:
      Executing /home/root/.profile.
      -sh: /release: No such file or directory
      No reserved DMA memory found on the linux boot command line.
      [   27.507843] opencpi: loading out-of-tree module taints kernel.
      [   27.514712] opencpi: dma_set_coherent_mask failed for device ffffffc06ce74800
      [   27.521903] opencpi: get_dma_memory failed in opencpi_init, trying fallback
      [   27.528893] NET: Registered protocol family 12
      Driver loaded successfully.
      OpenCPI ready for zynq.
      Loading bitstream
      Exiting for problem: error loading device pl:0: Can't open bitstream file '/home/root/opencpi/artifacts/testbias__base.bitz' for reading: No such file or directory(2)
      Bitstream load error

   ..

   ``% rm /home/root/.profile``

   ``% reboot``

..

.. _dev-OpenCPI-Staging-label:

OpenCPI Staging
---------------

.. _dev-Install-the-OpenCPI-framework-label:

Install the OpenCPI framework
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- To install the OpenCPI ``v2.2.0`` Framework

**IMPLEMENTATION:**

Download the ``v2.2.0`` tag of OpenCPI from the provided link, then run the default installation
that targets the host OS, which in this case is ``centos7``.

#. Clone the OpenCPI framework

   ``$ cd /home/user``

   ``$ git clone https://gitlab.com/opencpi/opencpi.git``

   ``$ cd opencpi``

   ``$ git checkout tags/v2.2.0``

#. Complete the steps in the :ref:`dev-Bug-Fix-to-the-framework-label` section of the APPENDIX

#. Install the framework  (Duration 30 min)

   ``$ cd /home/user/opencpi/``

   ``$ ./scripts/install-opencpi.sh``


.. _dev-Configure-a-host-terminal-for-OpenCPI-development-label:

Configure a host terminal for OpenCPI development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- As a convenience, below are the steps for configuring a Host terminal for the OpenCPI development
  environment

**IMPLEMENTATION:**

#. After the OpenCPI framework has been installed, source the OpenCPI framework setup script

   ``$ cd /home/user/opencpi``

   ``$ source cdk/opencpi-setup.sh -s``

#. Ensure that the environment is configured for the **desired version of Vivado** and its license
   file

   ``$ export OCPI_XILINX_VIVADO_VERSION=2018.3``

   ``$ export OCPI_XILINX_LICENSE_FILE=2100@r420``

   ``$ env | grep OCPI``

   ::

      $ env | grep OCPI
      OCPI_CDK_DIR=/home/user/opencpi/cdk
      OCPI_PREREQUISITES_DIR=/home/user/opencpi/prerequisites
      OCPI_ROOT_DIR=/home/user/opencpi
      OCPI_TOOL_ARCH=x86_64
      OCPI_TOOL_DIR=centos7
      OCPI_TOOL_PLATFORM=centos7
      OCPI_TOOL_PLATFORM_DIR=/home/user/opencpi/cdk/../project-registry/ocpi.core/exports/rcc/platforms/centos7
      OCPI_TOOL_OS_VERSION=c7
      OCPI_TOOL_OS=linux
      OCPI_XILINX_LICENSE_FILE=2100@r420
      OCPI_XILINX_VIVADO_VERSION=2018.3

  ..

.. _dev-Modifications-to-the-Install-and-Deploy-scripts-label:

Modifications to the Install and Deploy scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- By default, the ``testbias`` HDL assembly (Control + Data Plane) is built as part of the
  installation process for a given OSP, and this bitstream is included in the deployment of said
  OSP. The purpose of these modifications are to replace the ``testbias`` HDL assembly, so that,
  the ``canary`` Control Plane HDL assembly ``pattern_capture`` is built and its bitstream, and
  application (``pattern_capture.xml``) are deployed, thus making it the ``new`` default assembly
  and app for assessing the behavior of the Control Plane.

**IMPLEMENTATION:**

#. So that the ``canary`` Control Plane app (``pattern_capture.xml``) is included in the list of OASs
   that are exported to ``/home/user/opencpi/cdk/<rcc-platform>/sd*/opencpi/applications``, create
   a symbolic link within the ``/home/user/opencpi/projects/assets/applications/`` to
   ``pattern_capture.xml``

   ``$ cd /home/user/opencpi/projects/assets/applications/``

   ``$ ln -s pattern_capture/pattern_capture.xml pattern_capture.xml``

#. Edit the following scripts to target the assembly ``pattern_capture_asm``, rather than the
   ``testbias`` assembly:

   .. note::

      #. **It is recommended to perform a ``Find and Replace`` for all occurances of ``testbias``
         with ``pattern_capture`` in the files listed below. A syntax error in these files can be
         difficult to diagnose, therefore it is NOT recommended to simply comment out and replace
         the lines when making these edits.**

      #. **In a later section** (:ref:`dev-undo-edits-made-to-validate-hdl-control-plane-label`),
         **these edits will be reverted back to their original state, so that, the ``testbias``
         will be installed/deployed in support of enabling the Data Plane.**

   ..

   #. Edit the ``/home/user/opencpi/tools/scripts/export-platform-to-framework.sh`` to target
      ``pattern_capture_asm``

      FROM::

         tbz=projects/assets/exports/artifacts/ocpi.assets.testbias_${platform}_
         base.hdl.0.${platform}.bitz

      ..

      TO::

         tbz=projects/assets/exports/artifacts/ocpi.assets.pattern_capture_asm_${platform}_
         base.hdl.0.${platform}.bitz

      ..

   #. Edit the ``/home/user/opencpi/tools/scripts/ocpiadmin.sh`` to target ``pattern_capture_asm``

      FROM::

         ocpidev -d projects/assets build --hdl-platform=$platform hdl
         ${minimal:+--workers-as-needed} assembly testbias

      ..

      TO::

         ocpidev -d projects/assets build --hdl-platform=$platform hdl
         ${minimal:+--workers-as-needed} assembly pattern_capture_asm

      ..

      **AND**

      FROM::

         echo "HDL platform \"$platform\" built, with one HDL assembly (testbias) built for
         testing."

      ..

      TO::

         echo "HDL platform \"$platform\" built, with one HDL assembly (pattern_capture_asm) built
         for testing."

      ..

   #. Edit the ``/home/user/opencpi/tools/scripts/deploy-platform.sh`` to target
      ``pattern_capture_asm``

      FROM::

         cp $verbose -L ../projects/assets/hdl/assemblies/testbias/container-testbias_$
         {hdl_platform}_base/target-*.bitz \
         $sd/opencpi/artifacts

      ..

      TO::

         cp $verbose -L ../projects/assets/hdl/assemblies/pattern_capture_asm/container-
         pattern_capture_asm_${hdl_platform}_base/target-*.bitz \
         $sd/opencpi/artifacts

      ..

.. _dev-Setup-the-Software-cross-compiler-label:

Setup the Software cross-compiler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- To establish the software cross-complier

- To setup the OpenCPI functionality of the ``ZynqReleases`` and ``git`` Xilinx directories

**IMPLEMENTATION:**

The following commands are outlined in the OpenCPI Installation Guide here:
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf

#. Download the prebuilt Linux image for the ``zcu104`` board platform

#. Go to the Xilinx Wiki page at https://xilinx-wiki.atlassian.net/wiki/spaces/A/overview

#. Select the ``Linux Prebuild Images`` page on the side ribbon

#. Select ``2018.3 Release``

#. Navigate/scroll to the ``Download`` section

#. Download the ``2018.3-zcu104-release.tar.xz``

#. Setup ``Xilinx/ZynqReleases/``

   ``$ sudo mkdir -p /opt/Xilinx/ZynqReleases/2018.3/``

   ``$ cd /home/user/Downloads``

   ``$ sudo cp 2018.3-zcu104-release.tar.xz /opt/Xilinx/ZynqReleases/2018.3``

   ``$ sudo chown -R <user>:users /opt/Xilinx/ZynqReleases``

   Example: ``$ sudo chown -R smith:users /opt/Xilinx/ZynqReleases``

   .. note::

      **This may require adjusting the permissions for ``/opt/Xilinx/ZynqReleases`` or
      its subdirectories.**

   ..

#. Setup ``Xilinx/git/``

   ``$ sudo mkdir -p /opt/Xilinx/git``

   ``$ cd /opt/Xilinx/git``

   ``$ sudo git clone --branch xilinx-v2018.3 https://github.com/Xilinx/linux-xlnx.git``

   ``$ sudo git clone --branch xilinx-v2018.3 https://github.com/Xilinx/u-boot-xlnx.git``

   ``$ sudo chown -R <user>:users /opt/Xilinx/git``

   Example: ``$ sudo chown -R smith:users /opt/Xilinx/git``

   .. note::

      **This may require adjusting the permissions for ``/opt/Xilinx/git`` or its
      subdirectories.**

   ..

.. _dev-Install-zcu104-HDL-Platform-label:

Install zcu104 HDL Platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL**

- To install for a build-in HDL platform that targets the same Xilinx device family as the ``Z3u``.
  This will ensure that all of the HDL assets are built for the correct target device family
  (``zynq_ultra``).

**IMPLEMENTATION:**

#. **Install: zcu104 (an HDL platform)**

   ``$ ocpiadmin install platform zcu104``

   .. note::

      **Estimated time ~14+ Hours**
   ..

.. _dev-Create/Install-xilinx18_3_aarch64-RCC-Platfrom-label:

Create/Install xilinx18_3_aarch64 RCC Platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL**

- The ``z3u`` HDL Platform requires the use of the RCC platform ``xilinx18_3_aarch64``. Since the
  ``xilinx18_3_aarch64`` RCC Platform is not implemented in this version (v2.2.0) of OpenCPI,
  the user must create the RCC Platform for building the OpenCPI run-time utilities against
  ``xilinx18_3_aarch64``.

**IMPLEMENTATION:**

#. **Create new RCC Platform:** ``core/rcc/platforms/xilinx18_3_aarch64`` **to target the Z3u**

   #. ``$ cd /home/user/opencpi/projects/core/rcc/platforms/``

   #. Copy the core RCC platform ``xilinx19_2_aarch64`` directory and rename it
      ``xilinx18_3_aarch64``

      ``$ cp -rf xilinx19_2_aarch64/ ./xilinx18_3_aarch64``

   #. ``$ cd xilinx18_3_aarch64/``

   #. Ensure that the newly created ``xilinx18_3_aarch64/`` is void of old artifacts,
      i.e. remove ``gen/`` and ``lib/`` that may have been copied from xilinx19_2_aarch64

   #. Change file names in ``xilinx18_3_aarch64/`` from ``19_2`` to ``18_3``

   #. Edit the contents of ``xilinx18_3_aarch64.mk`` per the below:

      ::

         #OcpiXilinxLinuxRepoTag:=xilinx-v2019.2.01

         include $(OCPI_CDK_DIR)/include/xilinx/xilinx-rcc-platform-definition.mk
         OcpiCXXFlags+=-fno-builtin-memset -fno-builtin-memcpy
         OcpiCFlags+=-fno-builtin-memset -fno-builtin-memcpy
         OcpiPlatformOs:=linux
         OcpiPlatformOsVersion:=18_3
         OcpiPlatformArch:=aarch64

   #. Unregister and reregister the project:

      ``$ cd /home/user/opencpi/projects/core``

      ``$ ocpidev unregister project``

      ``$ ocpidev register project``

   #. Verify that the ``xilinx18_3_aarch64`` project has been registered:

      ``$ cd /home/user/opencpi``

      ``$ ocpidev show platforms``

#. **Install: xilinx18_3_aarch64 (an RCC platform)**

   ``$ ocpiadmin install platform xilinx18_3_aarch64``

   .. note::

      If the installation fails with a message similar to the below, the host's version of
      uboot tools is too new and must be downgraded, per the following

      ::

         Extracting root FS for release 2018.3 platform zcu104 in gen/release-artifacts/zcu104 (for gdbserver and set sysroot)
         Extracting uramdisk from image.ub
         dumpimage: invalid option -- 'i'
         Usage: dumpimage -l image
         -l ==> list image header information
         dumpimage [-T type] [-p position] [-o outfile] image
         -T ==> declare image type as 'type'
         -p ==> 'position' (starting at 0) of the component to extract from image
         -o ==> extract component to file 'outfile'
         dumpimage -h ==> print usage information and exit
         dumpimage -V ==> print version information and exit
         gzip: /tmp/tmp.MWuxtyH8tU/rootfs.cpio.gz: No such file or directory
         make: *** [gen/release-artifacts.done] Error 1
         make: Leaving directory `/home/user/opencpi/projects/core/rcc/platforms/xilinx18_3_aarch64'

      ..


      ``$ sudo yum downgrade uboot-tools 2018.09``

   ..

.. _dev-Deploy-platforms-zcu104-xilinx18_3_aarch64-label:

Deploy platforms: zcu104, xilinx18_3_aarch64
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- ``Known good`` HDL and RCC platforms are installed and deployed to aid in the development of the
  OSP for the Z3u. They are used to validate the installation of the framework and to ensure that
  the run-time utilities perform on the zcu104 as expected prior to enabling the Z3u.

**IMPLEMENTATION:**

.. note::

   **The z3u HDL platform will not be installed here as it has not yet been developed.**

..

#. **Deploy: zcu104 with xilinx18_3_aarch64**

   ``$ ocpiadmin deploy platform xilinx18_3_aarch64 zcu104``

.. _dev-Benchmark-testing-the-OpenCPI-zcu104-OSP-label:

Benchmark testing the OpenCPI zcu104 OSP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Gain a benchmark understanding of build-time and run-time utilities, as they are performed for
  the OpenCPI ``zcu104`` HDL platform when paired with SW RCC platform ``xilinx18_3_aarch64``.

**IMPLEMENTATION:**

#. Build ``known good`` HDL and RCC platforms to aid in the development of the OSP.
   *There is a calculated risk using a new RCC platform for this step.*

#. Understand the impact of the contents of the ``zcu104.exports`` file by reviewing the outputs of
   install/deploy of the ``zcu104``

#. Build the ``canary`` **Control Plane** (CP) HDL bitstreams and run its application

#. Build the ``canary`` **Data Plane** (DP) HDL bitstreams and run its application

#. Build the Component Unit Tests and run them on the ``zcu104`` to obtain benchmark performance
   metrics. These benchmark performance metrics for the ``zcu104`` are outlined in the
   :ref:`dev-Component-Unit-Test-results-table-label` section.

.. _dev-Create-an-OpenCPI-project-for-the-Z3u-label:

Create an OpenCPI project for the Z3u
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- To create a skeleton project directory for the OSP and add to the project-registry

**IMPLEMENTATION:**

#. Create a project, under ``/home/user/opencpi/projects/osps``

   ``$ cd /home/user/opencpi/projects/osps/``

   ``$ ocpidev create project ocpi.osp.epiq_solutions``

   ``$ cd ocpi.osp.epiq_solutions``

#. Create ``Project.mk``:

   ::

      PackageName=osp.epiq_solutions
      PackagePrefix=ocpi
      ProjectDependencies=ocpi.platform ocpi.assets
      ComponentLibraries+=misc_comps util_comps dsp_comps comms_comps

#. Register the project

   ``$ ocpidev register project``


#. Confirm that the ``ocpi.osp.epiq_solutions`` project is registered

   ``$ ocpidev show registry``

   ::

      Project registry is located at: /home/user/opencpi/project-registry
      -----------------------------------------------------------------------------------------------------------
      | Project Package-ID          | Path to Project                                            | Valid/Exists |
      | ----------------------------| -----------------------------------------------------------| -------------|
      | ocpi.core                   | /home/user/opencpi/projects/core                           | True         |
      | ocpi.tutorial               | /home/user/opencpi/projects/tutorial                       | True         |
      | ocpi.assets_ts              | /home/user/opencpi/projects/assets_ts                      | True         |
      | ocpi.assets                 | /home/user/opencpi/projects/assets                         | True         |
      | ocpi.platform               | /home/user/opencpi/projects/platform                       | True         |
      | ocpi.osp.epiq_solutions     | /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions   | True         |
      -----------------------------------------------------------------------------------------------------------

   ..



.. _dev-Enable-OpenCPI-HDL-Control-Plane-label:

Enable OpenCPI HDL Control Plane
-------------------------------


.. _dev-Configure-PS-for-CP-label:

Conigure PS for CP
^^^^^^^^^^^^^^^^^^

**GOAL:**

- Obtain a **Processing System** (PS) core IP which is precisely configured for the Z3u, that
  will be wrapped or normalized into OpenCPI signaling and made available to be instanced
  in the Platform Worker. The steps to acheive this goal start by leveraging the Z3u vendor
  reference design to isolate the Processing System (PS) core IP's source code.

- Configure the PS core IP to enable and configure signals/ports, per the requirements
  of the OpenCPI HDL Control Plane control software for the Zynq UltraScale+ MPSoC devices:

   - Clock(s)

   - Reset(s)

   - An AXI Master interface - Memory mapping must match with that defined in ``HdlZynq.h``

- The product of this section is the ``minimal`` PS core IP HDL primitive that
  supports the Control Plane.

**IMPLEMENTATION:**

#. Complete the steps outlined in the APPENDIX section: :ref:`dev-Build-Vendor-Reference-Design-Bitstream-label`

#. ``$ cd /home/user/vendor_reference_design_package_gui_saved``

#. ``$ source /opt/Xilinx/Vivado/2018.3/settings64.sh``

#. ``$ vivado sidekiq_z3u_pdk.xpr &``

#. File -> Project -> Save As

   - Change Project Location: ``/home/user/z3u_ocpi_cp``

   - Set Project name: ``sidekiq_z3u_ocpi_cp``

   - Check box for ``Import all files to the new project``

   - Click ``OK``

#. The newly saved project ``sidekiq_z3u_ocpi_cp``, should now be open.

#. To be sure that no stale files are present, close the project and Vivado,
   then reopen the project.

#. ``$ cd /home/user/z3u_ocpi_cp``

#. ``$ vivado sidekiq_z3u_ocpi_cp.xpr &``

#. Isolate the ``zynq_system.bd`` by removing all other source files of the project

   #. In the ``Sources`` pane, use the ``Search`` bar to search for the following:
      ``zynq_system_i``. Highlight the ``zynq_system_i``, but DO NOT delete it!

   #. Remove all other source files

   #. Right click the ``Sources`` pane and select ``Refresh Hierarchy``

#. Open the Block Design for editing

   #. Remove all modules except the Zynq UltraScale+ MPSoC IP ``zynq_ultra_ps_e_0``

   #. Remove all connections

   #. Edit the PS core IP (Double Click ``zynq_ultra_ps_e_0``):

         #. **Disable GPIO/EMIO:**

            I/O Configuration -> Low-Speed -> I/O Peripherals -> GPIO -> GPIO EMIO

         #. **Disable pl_clk1:**

            Clock Configuration -> Output Clocks (Tab) -> Low Power Domain Clocks ->
            PL Fabric Clocks -> PL1

         #. **Disable PL to PS Interrupts:**

            PS-PL Configuration -> General -> Interrupts -> PL to PS -> IRQ0[0-7] -> 0

         #. **Disable the second Master Interface AXI HPM1 FPD:**

            PS-PL Configuration -> PS-PL Interfaces -> Master Interface -> AXI HPM1 FPD

         #. **Configure the Master Interface AXI HPMO FPD for 32 bit**

            PS-PL Configuration -> PS-PL Interfaces -> Master Interface -> AX0 HPM1 FPD Data Width
            -> 32

         #. **Disable both Slave Interfaces Streaming-AXIS ports: AXI HP0/1 FPD**

            PS-PL Configuration -> PS-PL Interfaces -> Slave Interface -> AXI HP -> AX0 HP0 FPD

            PS-PL Configuration -> PS-PL Interfaces -> Slave Interface -> AXI HP -> AX0 HP1 FPD

   #. Remove all severed ports

   #. Make a connection from ``pl_clk0`` to ``maxihpm0_fpd_aclk``

   #. Perform ``Regenerate Layout``

   #. Perform ``Validate Design (F6)``

   #. Perform ``Generate Block Design``

#. In the ``Sources`` tab and ``Hierarchy`` view, right mouse click the ``zynq_system`` design
   and click ``Create HDL Wrapper``

   - The ``zynq_system_wrapper.v`` should should have an empty port declaration

#. Close Vivado

#. The Block Design should look as follows:

.. figure:: figures/z3u_ocpi_cp.png
   :alt: Z3u PS CP Block Design
   :align: center

   Z3u PS CP Block Design

..


.. _dev-Create-HDL-Primitive-for-CP-label:

Create HDL Primitive for CP
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Create an OpenCPI HDL primitive that wraps the Zynq UltraScale+ MPSoC PS core IP which has
  been configured per the settings of the ``Z3u``. As the ``zcu104`` OpenCPI HDL Platform targets
  the same device family, its HDL primitive module is used as a reference implementation for this
  task.

**IMPLEMENTATION:**

.. note::

   **CODEBLOCK: The code block for the various files that make up the HDL Primitive can be found in
   the following directory of the ocpi.osp.epiq_solutions repository:**

   ::

      ocpi.osp.epiq_solutions/hdl/platform/z3u/doc/codeblocks/Enable_OpenCPI_Control-Plane/Primitive/

   ..

..

#. Setup terminal for OpenCPI development

   ``$ cd /home/user/opencpi``

   ``$ source cdk/opencpi-setup.sh -s``

   ``$ export OCPI_XILINX_VIVADO_VERSION=2018.3``

#. Create an OpenCPI HDL primitive library, named ``zynq_ultra_z3u``

   ``$ cd projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev create hdl primitive library zynq_ultra_z3u``

#. From the Vivado project modified in  :ref:`dev-Configure-PS-for-CP-label`, which is specific to
   using the vendor's reference design for configuring the PS core IP for the ``Z3u``, browse to
   the generated artifacts directory, and copy them into the newly created OpenCPI HDL primitive
   library.

   ``$ cd /home/user/z3u_ocpi_cp/sidekiq_z3u_ocpi_cp.srcs/sources_1/bd/zynq_system/ip/``

   ``$ cp -rf zynq_system_zynq_ultra_ps_e_0_0/ <ocpi.osp.epiq_solutions>/hdl/primitives/zynq_ultra_z3u``

#. Since the ``z3u`` is very similar to the ``zcu104``, simply copy and rename a couple files from
   the ``platform/hdl/primitive/zynq_ultra`` HDL primitive library into the ``zynq_ultra_z3u`` and
   edit as needed.

   ``$ cd /home/user/opencpi/projects/platform/hdl/primitives/zynq_ultra/``

   ``$ cp zynq_ultra_pkg.vhd  <ocpi.osp.epiq_solutions>/hdl/primitives/zynq_ultra_z3u/zynq_ultra_z3u_pkg.vhd``

   ``$ cp zynq_ultra_ps.cpp_vhd <ocpi.osp.epiq_solutions>/hdl/primitives/zynq_ultra_z3u/zynq_ultra_z3u_ps.vhd``

   .. note::

      **This guide does not perform the C++ preprocessing on the zynq_ultra_ps.cpp_vhd, that is
      described in the platform/hdl/primitives/zynq_ultra/Makefile. All C++ preprocessing will be
      removed in subsequent steps.**
   ..

#. Edit the HDL package ``zynq_ultra_z3u_pkg.vhd``

   #. Change package name from ``zynq_ultra_pkg`` to ``zynq_ultra_z3u_pkg``

   #. Reduce the number of Master ports that are supported from 2 to 1.

      From: ``constant C_M_AXI_HP_COUNT : natural := 2``

      To: ``constant C_M_AXI_HP_COUNT : natural := 1``

   #. Change primitive component name from ``zynq_ultra_ps`` to ``zynq_ultra_z3u_ps``

   #. Comment out the ``s_axi_hp_in`` and ``s_axi_hp_out ports``

      .. note::

	 These are for the Data Plane and will be added back into the design in a later section.

      ..

#. Edit the ``zynq_ultra_z3u_ps.vhd``, to remove all C++ preprocessing code and to normalize the
   interface of the generated PS core IP to OpenCPI Control Plane signaling.

   .. note::

      This file edit is very intricate and in depth. As you read through this section it is
      encouraged that you use a comparison tool and compare the CODEBLOCK of this file (outlined at
      the top of this section) with the file that is currently in place in your project. This will
      give you the best overview of the step-by-step process in this section.

   ..

   #. Change library names from:

      from ``library zynq_ultra`` to ``library zynq_ultra_z3u``

      from ``zynq_ultra.zynq_ultra_pkg.all`` to ``zynq_ultra_z3u.zynq_ultra_z3u_pkg.all``

   #. Change entity name:

      from ``zynq_ultra_ps`` to ``zynq_ultra_z3u_ps``

   #. Comment out any lines containing:

      ``s_axi_hpi_in``, ``s_axi_hp_out``

   #. Change architecture name:

      from ``zynq_ultra_ps`` to ``zynq_ultra_z3u_ps``

   #. Change the component name:

      from ``PS8_WRAPPER_MODULE`` to ``zynq_system_zynq_ultra_ps_e_0_0``

   #. Remove the ``GENERIC`` ports:

   #. Remove all ports in the entity except for the following:

      #. ``maxihpm0_*``, ``saxihp0_*``, ``saxihp1_*``, ``saxihp2_*``, ``saxihp3_*``

      #. ``maxigp0_*``, ``saxigp2_*``, ``saxigp3_*``, ``saxigp4_*``, ``saxigp5_*``

      #. ``pl_resetn0``

      #. ``pl_clk0``

   #. Of the remaining ports in the entity, comment out the following  ports from the entity
      (these are for the Data Plane, and will be added back later):

      #. ``saxihp0_*``, ``saxihp1_*``, ``saxihp2_*``, ``saxihp3_*``

      #. ``saxigp2_*``, ``saxigp3_*``, ``saxigp4_*``, ``saxigp5_*``

   #. Replace the ``PS8_WRAPPER_MODULE`` that is in the architecture declaration and body with
      ``zynq_system_zynq_ultra_ps_e_0_0``

   #. In the ``PORT MAP`` perfrom the same removal and commenting as in the entity

      #. Remove the ``GENERIC`` ports

      #. Remove all ports in the entity except for the following:

         #. ``maxihpm0_*``, ``saxihp0_*``, ``saxihp1_*``, ``saxihp2_*``, ``saxihp3_*``

         #. ``maxigp0_*``, ``saxigp2_*``, ``saxigp3_*``, ``saxigp4_*``, ``saxigp5_*``

         #. ``pl_resetn0``

         #. ``pl_clk0``

   #. Of the remaining ports in the entity, comment out the following  ports from the entity
      (these are for the Data Plane, and will be added back later):

      #. ``saxihp0_*``, ``saxihp1_*``, ``saxihp2_*``, ``saxihp3_*``

      #. ``saxigp2_*``, ``saxigp3_*``, ``saxigp4_*``, ``saxigp5_*``

   #. All that should remain are the following ports:

      #. gm: for i in - to C_M_AXI_HP_COUNT-1 code block (Control Plane Ports)

      #. gs: for i in 0 to C_S_AXI_HP_COUNT-1 generate code block (Data Plane Ports)

      #. ``maxihpm0_fpd_aclk`` (Control Plane Clock - destination)

      #. ``maxigp0_*`` (Control Plane Ports)

      #. ``saxihp{0,1,2,3}_fpd_aclk`` (Data Plane Clocks - destination)

      #. ``saxigp{2,3,4,5}`` (Data Plane Ports)

      #. ``pl_resetn0``

      #. ``pl_clk0`` (Control Plane Clock - source)

   #. Of the remaining ports, the following ports listed should be commented out (these are for the
      Data Plane, and will be added back later)

      #. gs: for i in 0 to C_S_AXI_HP_COUNT-1 generate code block (Data Plane Ports)

      #. ``saxihp{0,1,2,3}_fpd_aclk`` (Data Plane Clocks)

      #. ``saxigp{2,3,4,5}`` (Data Plane Ports)

#. Update the primitive library's ``Makefile`` to specify all of the dependencies:

   ``/home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions/hdl/primitives/zynq_ultra_z3u/Makefile``

   ::

      Libraries=fixed_float ocpi axi sdp platform

      SourceFiles= \
          zynq_ultra_z3u_pkg.vhd \
          zynq_ultra_z3u_ps.vhd \
          zynq_system_zynq_ultra_ps_e_0_0/hdl/zynq_ultra_ps_e_v3_2_2.v \
          zynq_system_zynq_ultra_ps_e_0_0/synth/zynq_system_zynq_ultra_ps_e_0_0.v \
          zynq_system_zynq_ultra_ps_e_0_0/zynq_system_zynq_ultra_ps_e_0_0.xci

      OnlyTargets=zynq_ultra


.. _dev-Build-HDL-Primitive-with-CP-label:

Build HDL Primitive with CP
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Build the HDL Primitive that is instanced in the HDL Platform Worker

**IMPLEMENTATION:**

#. Return to the top of the project

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

#. Build the primitive library

   ``$ ocpidev build --hdl-target zynq_ultra``

   ::

      $ ocpidev build --hdl-target zynq_ultra
      No HDL platforms specified.  No HDL assets will be targeted.
      Possible HdlPlatforms are: alst4 isim matchstiq_z1 ml605 modelsim picoevb x4sim xsim zcu104 zcu106 zed zed_ise.
      ============== For library zynq_ultra_z3u:
      Building the zynq_ultra_z3u library for zynq_ultra (target-zynq_ultra/zynq_ultra_z3u) 0:()
       Tool "vivado" for target "zynq_ultra" succeeded.  0:00.02 at 16:51:42
      Creating directory ../lib/zynq_ultra_z3u for library zynq_ultra_z3u
      No previous installation for gen/zynq_ultra_z3u.libs in ../lib/zynq_ultra_z3u.
      Installing gen/zynq_ultra_z3u.libs into ../lib/zynq_ultra_z3u
      No previous installation for target-zynq_ultra/zynq_ultra_z3u.sources in target-zynq_ultra/zynq_ultra_z3u.
      Installing target-zynq_ultra/zynq_ultra_z3u.sources into target-zynq_ultra/zynq_ultra_z3u
      No previous installation for target-zynq_ultra/zynq_ultra_z3u in ../lib/zynq_ultra_z3u/zynq_ultra.
      Installing target-zynq_ultra/zynq_ultra_z3u into ../lib/zynq_ultra_z3u/zynq_ultra


.. _dev-Create-HDL-Platform-Worker-for-CP-label:

Create HDL Platform Worker for CP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   **CODEBLOCK: The code block for the various files that make up the HDL platform worker can be
   foundin the following directory of the ocpi.osp.epiq_solutions repository:**

   ::

      ocpi.osp.epiq_solutions/hdl/platforms/z3u/doc/codeblocks/Enable_OpenCPI_Control-Plane/Platform-Worker/

   ..

..

#. Create HDL Platform Worker

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev create hdl platform z3u``

#. Change directory to HDL Platform Worker

   ``$ cd hdl/platforms/z3u``

#. Copy ``zcu104.xml`` into the ``z3u`` platform worker directory and rename it ``z3u.xml``

   ``$ cp /home/user/opencpi/projects/platform/hdl/platforms/zcu104/zcu104.xml ./z3u.xml``

#. Edit the newly created ``z3u.xml``

   #. Change all references of ``zcu104`` to ``z3u``

   #. Delete all ``specproperty`` elements except for ``platform``, i.e. delete nLEDS, nSlots,
      nSwitches, slotNames.

   #. Comment out the Scalable-Data Plane interface, i.e. ``<sdp name=...>```

   #. Delete Property: ``useGP1``

   #. Comment out debug Properties: ``axi_error``, ``sdpDropCount``, ``debug_state``,
      ``debug_state1``, and ``debug_state2``

   #. Remove all signals

   #. Remove the slot declaration(s) and associated signals and comments

#. Copy ``zcu104.vhd`` into the z3u platform worker directory and rename it ``z3u.vhd``

   ``$ cp /home/user/opencpi/projects/platform/hdl/platforms/zcu104/zcu104.vhd ./z3u.vhd``

#. Edit the newly created ``z3u.vhd``

   #. Comment out Library: ``platform``

   #. Change Library: ``zynq_ultra`` to ``zynq_ultra_z3u``

   #. Change: ``zynq_ultra.zynq_ultra_pkg.all`` to ``zynq_ultra_z3u.zynq_ultra_z3u_pkg.all``

   #. Comment out Library: ``bsv`` and ``sdp``

   #. Change architecture: ``zcu104_worker`` to ``z3u_worker``

   #. Remove ``whichGP`` comments and constant

   #. Comment out the following signals:

      ``ps_s_axi_hp_in``, ``ps_s_axi_hp_out``, ``rst_n``, ``my_sdp_out``, ``my_sdp_out_data``,
      ``dbg_state``, ``dbg_state1``, ``dbg_state2``

   #. Remove signals: ``count`` ``ledbuf`` and ``cnt_t``

   #. Change ``ps : zynq_ultra_ps`` to ``ps : zynq_ultra_z3u_ps``

   #. Remove the ``useGP1`` comments

   #. Change: ``ps in.debug => (31 => useGP1, others => '0')`` to ``ps in.debug => (others => '0')``

   #. Comment out connections: ``s_axi_hp_in``, ``s_axi_hp_out``, ``zynq_ultra_out``,
      ``zynq_ultra_out_data``, ``props_out.sdpDropCount``

   #. Change : ``ps_m_axi_gp_out(whichGP)`` to ``ps_m_axi_gp_out(0)``

   #. Change : ``ps_m_axi_gp_in(whichGP)`` to ``ps_m_axi_gp_in(0)``

   #. Comment out the ``sdp2axi`` adapter module

   #. Remove connections: ``props_out.switches``, ``leds``

   #. Remove the comments and ``process`` associated with driving the LEDS

#. Create a constraints files named ``z3u.xdc`` and add the following clock constraint

   ::

      # OpenCPI additions to the above, which is unmodified from the original

      create_clock -name clk_fpga_0 -period 10.000 [get_pins -hier * -filter {NAME =~ /ps/U0/inst/PS8_i/PLCLK[0]}]
      set_property DONT_TOUCH true [get_cells "ftop/pfconfig_i/z3u_i/worker/ps/U0/inst/PS8_i"]

   ..

      .. warning::

         **Signal paths may be different if the HDL dependency modules are based on
         VHDL vs Verilog**

      ..

#. Copy the ``zcu104/Makefile`` to ``z3u/Makefile`` and edit it such that its contents match the
   provided CODE BLOCK

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions/hdl/platforms/z3u``

   ``$ cp /home/user/opencpi/projects/platforms/hdl/platforms/zcu104/Makefile ./``

#. Copy/rename the ``zcu104/zcu104.mk`` to ``z3u/z3u.mk`` and edit it such that its contents match
   the provided CODE BLOCK

   ``$ cp /home/user/opencpi/projects/platforms/hdl/platforms/zcu104/zcu104.mk ./z3u.mk``

#. Copy/rename the ``zcu104/zcu104.exports`` to ``z3u/z3u.exports`` and edit it such that its
   contents match the provided CODE BLOCK

   ``$ cp /home/user/opencpi/projects/platforms/hdl/platforms/zcu104/zcu104.exports ./z3u.exports``

#. Copy/rename the ``zcu104/98-zcu104.rules`` to ``z3u/98-z3u.rules`` and edit such that its
   contents match the provided CODE BLOCK

   ``$ cp /home/user/opencpi/projects/platforms/hdl/platforms/zcu104/98-zcu104.rules ./98-z3u.rules``

#. Create a ``z3u_bit.xdc``, which is to remain empty.

   .. note::

      When this file is void of contents, it signifies to Vivado that all defaults project settings are acceptable.

   ..

#. Create an ``sd_card`` directory

   #. Copy ``/home/user/opencpi/platforms/zynq/zynq_system.xml`` into the
      ``ocpi.osp.epiq_solutions/hdl/platforms/z3u/sd_card/`` rename the file ``system.xml``, and
      edit the file to look like the following::

         <opencpi>
             <container>
                 <rcc load='1'/>
                 <remote load='1'/>
                 <hdl load='1'>
                     <device name='PL:0' platform='z3u'/>
                 </hdl>
             </container>
             <transfer smbsize='128K'>
                 <pio load='1' smbsize='10M'/>
                 <dma load='1'/>
                 <socket load='1'/>
             </transfer>
         </opencpi>

   #. Create an ``artifacts`` sub-directory in the ``sd_card`` directory. This sub-directory will
      be filled with the boot artifacts that were a product of the
      :ref:`dev-Configure-Z3u-microSD-card-artifacts-label` section. This will allow the framework to
      properly utilize the ``ocpiadmin deploy`` command in a later section.

      ``$ cd sd_card/``

      ``$ mkdir artifacts``

      ``$ cd artifacts``

      ``$ cp /tmp/z3u/petalinux_bsp/z3u_user.itb ./image.ub``

      ``$ cp /tmp/z3u/petalinux_bsp/images/linux/rootfs.tar.gz ./``

.. _dev-Build-HDL-Platform-Worker-with-CP-enabled-label:

Build HDL Platform Worker with CP enabled
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Build the HDL Platform Worker and ``base`` Platform Configuration

- Verify that the HDL platform is recognized by the framework

**IMPLEMENTATION:**

#. **Build the HDL platform z3u**

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev build --hdl-platform z3u --rcc-platform xilinx18_3_aarch64``

#. Confirm that the z3u is recognized by the framework as a valid HDL platform target:

   ``$ ocpidev show platforms``

   ::

      | ------------------------------------------------------------------------------------------------------------------------------------
      | Platform           | Type | Package-ID                             | Target              | HDL Part                   | HDL Vendor |
      | ------------------ | ---- | -------------------------------------- | ------------------- | -------------------------- | ---------- |
      | centos7            | rcc  | ocpi.core.platforms.centos7            | linux-c7-x86_64     | N/A                        | N/A        |
      | xilinx18_3_aarch64 | rcc  | ocpi.core.platforms.xilinx18_3_aarch64 | linux-18_3-aarch64  | N/A                        | N/A        |
      | z3u                | hdl  | ocpi.osp.epiq_solutions.platforms.z3u  | zynq_ultra          | xczu3eg-1-sbva484i         | xilinx     |
      | zcu104             | hdl  | ocpi.platform.platforms.zcu104         | zynq_ultra          | xczu7ev-2-ffvc1156e        | xilinx     |


.. _dev-Install-and-Deploy-with-CP-enabled-label:

Install and Deploy with CP enabled
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- The goal of this section is to **install** and **deploy** the z3u.

- Installation of the ``Z3u`` includes building the HDL Container (i.e. bitstream) for verifying
  the Control Plane.

- Deploying the ``Z3u`` involves targeting a specific RCC Platform, which results in the gathering
  artifacts into a directory that can be copied onto the microSD card.

**IMPLEMENTATION:**

#. Setup the Software cross-compiler

   #. For this step, the :ref:`dev-Setup-the-Software-cross-compiler-label` section  performed all
      that was necessary.

   .. note::

      It is **NOT REQUIRED** to create a ``2018.3-z3u-release.tar.xz`` file. The ``sd_card/artifacts``
      allows the framework to properly populate boot artifacts during the ``ocpiadmin deploy``
      command.
   ..

#. Browse to top of the OpenCPI directory

   ``$ cd /home/user/opencpi``

#. Setup terminal for OpenCPI development

   ``$ source ./cdk/opencpi-setup.sh -s``

   ``$ export OCPI_XILINX_VIVADO_VERSION=2018.3``

#. **Install**: z3u (an HDL platform)

   ``$ ocpiadmin install platform z3u``

   .. note::

      Assuming the zcu104 has gone through the *install* process, this step is estimated to take ~30 minutes.

   ..

#. **Deploy**: z3u with xilinx18_3_aarch64

   ``$ ocpiadmin deploy platform xilinx18_3_aarch64 z3u``

#. **Populate the sd-card artifacts**

   #. Be sure that the :ref:`dev-Format-microSD-card-label` section is complete.

   #. ``$ cd /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64``

   #. ``$ sudo rm -rf /run/media/<user>/root/*``

   #. ``$ sudo rm -rf /run/media/<user>/boot/*``

   #. ``$ cp image.ub /run/media/<user>/boot/``

   #. ``$ sudo tar xvf rootfs.tar.gz -C /run/media/<user>/root/``

   #. ``$ sudo cp -RLp opencpi/ /run/media/<user>/root/home/root/``

   #. ``$ umount /dev/sda1`` and ``$ umount /dev/sda2``

.. _dev-HDL-CP-Verification-OpenCPI-Magic-Word-label:

HDL CP Verification: OpenCPI Magic Word
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- The ``Magic Word`` is a constant value that is located in the OpenCPI Scalable Control Plane
  infrastructure HDL module and spells out ``CPIxxOPEN`` in hexidecimal. Successfully reading this
  register value is the first verification step to determine if the OpenCPI HDL Control Plane
  is functioning correctly.

- As this step only requires devmem/devmem2 to be available on the embedded image, it does
  not require that the OpenCPI run-time utilities to have been cross-compiled, thus greatly
  simplifying the level of effort required for verification.

**IMPLEMENTATION:**

#. Be sure that the :ref:`dev-Install-and-Deploy-with-CP-enabled-label` section has been
   implemented, specifically the **Populate the sd-card artifacts** step.

#. Execute the :ref:`dev-Boot-Z3u-label` section.

#. To setup ``Standalone mode`` properly to target the ``pattern_capture_asm`` bitstream. For
   the next step (Execute the Standalone Mode setup section)  edit the ``mysetup.sh`` script as
   follows:

   FROM:

   ::

      echo Loading bitstream
        if   ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/testbias_$HDL_PLATFORM\_base.bitz; then
          echo Bitstream loaded successfully

   ..

   TO:

   ::

      echo Loading bitstream
        if   ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/pattern_capture_asm_$HDL_PLATFORM\_base.bitz; then
          echo Bitstream loaded successfully

   ..


#. Execute the :ref:`dev-Standalone-Mode-setup-label` section.

#. Perform the following commands to verify that the Control Plane is successfully enabled:

::

   % devmem 0xa8000000
   0x4F70656E
   % devmem 0xa8000004
   0x43504900
   %

.. _dev-HDL-CP-Verification-Pattern-Capture-application-label:

HDL CP Verification: Pattern Capture application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Setup the z3u with the OpenCPI runtime environment and run the ``canary`` Control Plane test
  application ``pattern_capture``

**IMPLEMENTATION:**

#. Be sure that the :ref:`dev-Install-and-Deploy-with-CP-enabled-label` section has been
   implemented, specifically the **Populate the sd-card artifacts** step.

#. Execute the :ref:`dev-Boot-Z3u-label` section.

#. To setup ``Standalone mode`` properly to target the ``pattern_capture_asm`` bitstream. For
   the next step (Execute the Standalone Mode setup section)  edit the ``mysetup.sh`` script as
   follows:

   FROM:

   ::

      echo Loading bitstream
        if   ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/testbias_$HDL_PLATFORM\_base.bitz; then
          echo Bitstream loaded successfully

   ..

   TO:

   ::

      echo Loading bitstream
        if   ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/pattern_capture_asm_$HDL_PLATFORM\_base.bitz; then
          echo Bitstream loaded successfully

   ..

#. Execute the :ref:`dev-Standalone-Mode-setup-label` section.

#. ``% ocpirun -v -x -d pattern_capture.xml``

::

   % cd /home/root/opencpi/applications
   % export OCPI_LIBRARY_PATH=../artifacts
   % ocpirun -v -d pattern_capture.xml
   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
     Instance  0 pattern_v2 (spec ocpi.assets.util_comps.pattern_v2) on hdl container 0: PL:0, using pattern_v2/a/pattern_v2 in ../artifacts/pattern_capture_asm_z3u_base.bitz dated Mon Jul 12 13:53:15 2021
     Instance  1 capture_v2 (spec ocpi.assets.util_comps.capture_v2) on hdl container 0: PL:0, using capture_v2/a/capture_v2 in ../artifacts/pattern_capture_asm_z3u_base.bitz dated Mon Jul 12 13:53:15 2021
   Application XML parsed and deployments (containers and artifacts) chosen [0 s 21 ms]
   Application established: containers, workers, connections all created [0 s 9 ms]
   Dump of all initial property values:
   Property   0: pattern_v2.dataRepeat = "true" (cached)
   Property   1: pattern_v2.numMessagesMax = "0x5" (parameter)
   Property   2: pattern_v2.messagesToSend = "0x5"
   Property   3: pattern_v2.messagesSent = "0x0"
   Property   4: pattern_v2.dataSent = "0x0"
   Property   5: pattern_v2.numDataWords = "0xf" (parameter)
   Property   6: pattern_v2.numMessageFields = "0x2" (parameter)
   Property   7: pattern_v2.messages = "{0x4,0xfb},{0x8,0xfc},{0xc,0xfd},{0x10,0xfe},{0x14,0xff}" (cached)
   Property   8: pattern_v2.data = "0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xb,0xc,0xd,0xe" (cached)
   Property  20: capture_v2.stopOnFull = "true" (cached)
   Property  21: capture_v2.metadataCount = "0x0"
   Property  22: capture_v2.dataCount = "0x0"
   Property  23: capture_v2.numRecords = "0x100" (parameter)
   Property  24: capture_v2.numDataWords = "0x400" (parameter)
   Property  25: capture_v2.numMetadataWords = "0x4" (parameter)
   Property  26: capture_v2.metaFull = "false"
   Property  27: capture_v2.dataFull = "false"
   Property  28: capture_v2.stopZLMOpcode = "0x0" (cached)
   Property  29: capture_v2.stopOnZLM = "false" (cached)
   Property  30: capture_v2.stopOnEOF = "true" (cached)
   Property  31: capture_v2.totalBytes = "0x0"
   Property  32: capture_v2.metadata = "{0xfb000004,0x2961212f,0x2961212f,0xfb},{0xfc000008,0x2961212f,0x2961212f,0xfb},{0xfd00000c,0x29612206,0x29612206,0xfb},{0xfe000010,0x296122dd,0x296122dd,0xfb},{0xff000014,0x296123b4,0x296123b4,0xfb},{0x0}"
   Property  33: capture_v2.data = "0x0,0x0,0x1,0x0,0x1,0x2,0x0,0x1,0x2,0x3,0x0,0x1,0x2,0x3,0x4,0x0"
   Application started/running [0 s 8 ms]
   Waiting for application to finish (no time limit)
   Application finished [0 s 0 ms]
   Dump of all final property values:
   Property   0: pattern_v2.dataRepeat = "true" (cached)
   Property   2: pattern_v2.messagesToSend = "0x0"
   Property   3: pattern_v2.messagesSent = "0x5"
   Property   4: pattern_v2.dataSent = "0xf"
   Property   7: pattern_v2.messages = "{0x4,0xfb},{0x8,0xfc},{0xc,0xfd},{0x10,0xfe},{0x14,0xff}" (cached)
   Property   8: pattern_v2.data = "0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xb,0xc,0xd,0xe" (cached)
   Property  20: capture_v2.stopOnFull = "true" (cached)
   Property  21: capture_v2.metadataCount = "0x5"
   Property  22: capture_v2.dataCount = "0xf"
   Property  26: capture_v2.metaFull = "false"
   Property  27: capture_v2.dataFull = "false"
   Property  28: capture_v2.stopZLMOpcode = "0x0" (cached)
   Property  29: capture_v2.stopOnZLM = "false" (cached)
   Property  30: capture_v2.stopOnEOF = "true" (cached)
   Property  31: capture_v2.totalBytes = "0x3c"
   Property  32: capture_v2.metadata = "{0xfb000004,0x403e8761,0x403e8761,0x13b},{0xfc000008,0x403e8838,0x403e8761,0x13b},{0xfd00000c,0x403e8838,0x403e8838,0x13b},{0xfe000010,0x403e890f,0x403e890f,0x13b},{0xff000014,0x403e8abc,0x403e89e6,0x13b},{0x0}"
   Property  33: capture_v2.data = "0x0,0x0,0x1,0x0,0x1,0x2,0x0,0x1,0x2,0x3,0x0,0x1,0x2,0x3,0x4,0x0"


.. _dev-Enable-OpenCPI-HDL-Data-Plane-label:

Enable OpenCPI HDL Data Plane
-----------------------------


.. _dev-Configure-PS-for-DP-label:

Configure PS for DP
^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Modify the PS core IP from the :ref:`dev-Enable-OpenCPI-HDL-Control-Plane-label` section, to add the
  ports necessary to support enabling the OpenCPI Data Plane

- Re-generate the PS core IP output products

- Build, Run/Verify the ``canary`` Data Plane application: ``testbias``

- Build, Run/Verify another application which requires the Data Plane, but is more complicated:
  FSK ``filerw``

**IMPLEMENTATION:**

#. These steps continue with the completion of the :ref:`dev-Configure-PS-for-CP-label`
   section.

#. ``$ cd /home/user/z3u_ocpi_cp/``

#. ``$ source /opt/Xilinx/Vivado/2018.3/settings64.sh``

#. ``$ vivado sidekiq_z3u_ocpi_cp.xpr &``

#. File -> Project -> Save As

   - Change Project Location: ``/home/user/z3u_ocpi_dp``

   - Set Project name: ``sidekiq_z3u_ocpi_dp``

   - Check box for ``Import all files to the new project``

   - Click OK

#. The newly saved project ``sidekiq_z3u_ocpi_dp``, should now be open.

#. Enable the Slave High Performance ports of the PS core IP

   #. Open the Block Design

   #. Double-click the ``zynq_ultra_ps_e_0`` IP Block

   #. Click on the PS-PL Configuration

      #. ``PS-PL Interface`` -> ``Slave Interface`` -> ``AXI HP`` -> Enable the following AXI HP
         Ports:

         ``AXI HP0 FPD``, ``AXI HP1 FPD``, ``AXI HP2 FPD``, ``AXI HP3 FPD``

      #. From the same location expand each of the enabled ``AXI HP* FPD`` Ports to select a
         ``64 bit`` ``AXI HP FPD* Data Width`` -> Select ``OK``

      #. For each of the Slave AXI HP port clocks ( ``saxihp*_fpd_aclk``), perform ``Make
         External``

#. Perform ``Regenerate Layout``

#. Perform ``Validate Design (F6)``

#. Perform ``Flow Navigator window`` -> ``IP INTEGRATOR`` -> ``Generate Block Design``

#. The Block Design should look as follows:

.. figure:: figures/z3u_ocpi_dp.png
   :alt: Z3u PS DP Block Design
   :align: center

   Z3u PS DP Block Design

..


.. _dev-Configure-HDL-Primitive-for-DP-label:

Configure HDL Primitive for DP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Edit the OpenCPI HDL primitive library source to support the Slave High-Performance (HP) ports
  that were made available in the previous section.

**IMPLEMENTATION:**

.. note::

   **CODEBLOCK: The "finalized" code block of various files mentioned in this section can be
   found at:**

   ::

      ocpi.osp.epiq_solutions/hdl/primitives/zynq_ultra_z3u/

   ..

..

#. At the start of this effort, perform a clean within the OSP directory to ensure that no stale
   files exist

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev clean``

#. In an effort to avoid stale content, remove the current PS core IP before copying
   over the updated version

   ``$ cd /home/user/projects/osps/ocpi.osp.epiq_solutions/hdl/primitives/zynq_ultra_z3u``

   ``$ rm -rf zynq_system_zynq_ultra_ps_e_0_0``

#. From :ref:`dev-Configure-PS-for-DP-label`, copy the updated ``zynq_system_zynq_ultra_ps_e_0_0``
   directory into the ``ocpi.osp.epiq_solutions`` HDL primitive directory

   ::

      $ cp -rf /home/user/z3u_ocpi_dp/sidekiq_z3u_ocpi_dp.srcs/sources_1/bd/zynq_system/ip/zynq_system_zynq_ultra_ps_e_0_0/ /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions/hdl/primitives/zynq_ultra_z3u/

   ..

#. Edit the ``zynq_ultra_z3u_pkg.vhd`` file to include the newly enabled Slave HP ports

   #. In the entity, uncomment the ``s_axi_hp_in`` and ``s_axi_hp_out`` ports

#. Edit the ``zynq_ultra_z3u_ps.vhd`` file to enable the newly enabled Slave HP ports

   #. In the entity, uncomment the ``s_axi_hp_in`` and ``s_axi_hp_out`` ports

   #. Uncomment all other ports which included:

      #. ``saxihp0_*``, ``saxihp1_*``, ``saxihp2_*``, ``saxihp3_*``

      #. ``saxigp2_*``, ``saxigp3_*``, ``saxigp4_*``, ``saxigp5_*``

   #. Uncomment the ``gs: for i in 0 to C_S_AXI_HP_COUNT-1 generate`` code block


.. _dev-Build-HDL-Primitive-with-DP-label:

Build HDL Primitive with DP
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Build the HDL Primitive that implements the DP and which is instanced in the z3u HDL Platform
  Worker

**IMPLEMENTATION:**

#. Return to the top of the project

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

#. **Build the primitive library**

   ``$ ocpidev build --hdl-target zynq_ultra``

   ::

      $ ocpidev build --hdl-target zynq_ultra
      No HDL platforms specified.  No HDL assets will be targeted.
      Possible HdlPlatforms are: alst4 alst4x isim matchstiq_z1 ml605 modelsim x4sim xsim zcu104 zcu104 zcu104_ise.
      make[1]: Entering directory `/home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions'
      make[1]: Leaving directory `/home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions'
      ============== For library zynq_ultra_z3u:
      Building the zynq_ultra_z3u library for zynq_ultra (target-zynq_ultra/zynq_ultra_z3u) 0:()
       Tool "vivado" for target "zynq_ultra" succeeded.  0:00.02 at 14:13:29
      Creating directory ../lib/zynq_ultra_z3u for library zynq_ultra_z3u
      No previous installation for gen/zynq_ultra_z3u.libs in ../lib/zynq_ultra_z3u.
      Installing gen/zynq_ultra_z3u.libs into ../lib/zynq_ultra_z3u
      No previous installation for target-zynq_ultra/zynq_ultra_z3u.sources in target-zynq_ultra/zynq_ultra_z3u.
      Installing target-zynq_ultra/zynq_ultra_z3u.sources into target-zynq_ultra/zynq_ultra_z3u
      No previous installation for target-zynq_ultra/zynq_ultra_z3u in ../lib/zynq_ultra_z3u/zynq_ultra.
      Installing target-zynq_ultra/zynq_ultra_z3u into ../lib/zynq_ultra_z3u/zynq_ultra


.. _dev-Configure-HDL-Platform-Worker-for-DP-label:

Configure HDL Platform Worker for DP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Edit the HDL Platform Worker files in order to implement the Slave High-Performance (HP) ports
  that have been made available to the ZynqMP Processing System.

**IMPLEMENTATION:**

.. note::

   **CODEBLOCK: The codeblock of various files mentioned in this section can be found at:**

   ::

      ocpi.osp.epiq_solutions/hdl/platforms/z3u/doc/codeblocks/Enable_OpenCPI_Data-Plane/Platform-Worker/

   ..

..

#. Edit the ``z3u.xml`` file

   #. Uncomment the ``<sdp name='zynq_ultra' master='true' count='4'/>``

   #. Uncomment the following Properties: ``axi_error``, and ``sdpDropCount``

   #. Leave the ``debug_state*`` properties commented out

#. Edit the ``z3u.vhd`` file:

   #. Uncomment the ``library sdp``

   #. Uncomment the newly created ``Slave HP`` signals: ``ps_m_axi_gp_in``, and ``ps_m_axi_gp_out``

   #. Uncomment the sdp signals: ``my_sdp_out``, and ``my_sdp_out_data``

   #. Leave the ``dbg_state*`` signals commented out

   #. Uncomment the ``s_axi_hp_in`` and ``s_axi_hp_out`` signals in the ``ps : zynq_ultra_z3u_ps``
      code block

   #. Uncomment the ``zynq_ultra_out`` and ``zynq_ultra_out_data`` signals in the ``cp : axi_...``
      code block

   #. Uncomment the generate block for the ``sdp2axi adapter``, leave the ``dbg_state*`` signals
      commented out


.. _dev-Build-HDL-Platform-Worker-with-DP-enabled-label:

Build HDL Platform Worker with DP enabled
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. **Build the HDL Platform Worker and "base" Platform Configuration**

   ``$ cd /home/user/projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev build --hdl-platform z3u --rcc-platform xilinx18_3_aarch64``


.. _dev-Undo-edits-made-to-validate-HDL-Control-Plane-label:

Undo edits made to validate HDL Control Plane
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Initially in support of validating the HDL platform for Control Plane ONLY, several scripts
  were modified to build and deploy the ``canary`` Control Plane bitstream (pattern_capture). The
  purpose of this section is to revert those changes such that the ``canary`` Data Plane bitstream
  (testbias) will be installed (i.e. built) and deployed for the targeted HDL platform.

**IMPLEMENTATION:**

#. ``$ cd /home/user/opencpi``

#. ``$ git checkout tools/scripts/deploy-platform.sh``

#. ``$ git checkout tools/scripts/export-platform-to-framework.sh``

#. ``$ git checkout tools/scripts/ocpiadmin.sh``


.. _dev-Install-and-Deploy-with-DP-enabled-label:

Install and Deploy with DP enabled
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- **Install** and **deploy** the z3u for verifying the DP.

  - Installation of the ``Z3u`` includes building the HDL Container (i.e. bitstream) for verifying
    the ``Data Plane``.

  - Deploying the ``Z3u`` involves targeting a specific RCC Platform, which results in the gathering
    artifacts into a directory that can be copied onto the microSD card.

- This section simply installs/builds the ``testbias`` bitstream and deploys (i.e. updates) the
  contents of the ``cdk/z3u/sdcard-xilinx18_3_aarch64/opencpi/artifacts/.bitz``

**IMPLEMENTATION:**

#. Setup the Software cross-compiler

   #. For this step, section :ref:`dev-Install-and-Deploy-with-CP-enabled-label` performed all that was
      necessary.

      .. note::

         **At this point in the development, there is NO NEED to create a
         2018.3-z3u-release.tar.xz file.**

      ..

#. Remove the old ``Z3u`` installation

   ``$ cd /home/user/opencpi/cdk``

   ``$ rm -rf z3u/``

#. Browse to the top of the OpenCPI directory

   ``$ cd /home/user/opencpi``

#. **Install**: z3u (an HDL platform)

   .. note::

      Estimated time ~30 minutes**
   ..

   ``$ ocpiadmin install platform z3u``

#. **Deploy**: z3u with xilinx18_3_aarch64

   ``$ ocpiadmin deploy platform xilinx18_3_aarch64 z3u``

#. **Populate the sd-card artifacts**

   #. Be sure that the :ref:`dev-Format-microSD-card-label` section is complete.

   #. ``$ cd /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64``

   #. ``$ sudo rm -rf /run/media/<user>/root/*``

   #. ``$ sudo rm -rf /run/media/<user>/boot/*``

   #. ``$ cp image.ub /run/media/<user>/boot/``

   #. ``$ sudo tar xvf rootfs.tar.gz -C /run/media/<user>/root/``

   #. ``$ sudo cp -RLp opencpi/ /run/media/<user>/root/home/root/``

   #. ``$ umount /dev/sda1`` and ``$ umount /dev/sda2``


.. _dev-HDL-DP-Verification-testbias-application-label:

HDL DP Verification: testbias application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- To successfully execute the ``canary`` HDL Data Plane application on the embedded platform.
  ``Success`` is defined as the application ran to completion and the md5sum of the input data
  vs the output data of the testbias application match, when no bias is applied to the data, i.e.
  bias worker property biasValue=0.

.. note::

   **The** :ref:`dev-Component-Unit-Test-results-table-label` **section in the
   APPENDIX contains the verfication test results of the z3u board.**

..

**IMPLEMENTATION:**

#. Be sure that the :ref:`dev-Install-and-Deploy-with-DP-enabled-label` section has been
   implemented, specifically the **Populate the sd-card artifacts** step.

#. Execute the :ref:`dev-Boot-Z3u-label` section.

#. Execute the :ref:`dev-Standalone-Mode-setup-label` section.

#. Run DP application: ``testbias``

   #. ``# cd /home/root/opencpi/applications``

   #. ``# export OCPI_LIBRARY_PATH=../artifacts:../xilinx18_3_aarch64/artifacts``

   #. ``# export OCPI_DMA_CACHE_MODE=0`` (required if FOSS version is <v2.2.0)

   #. Confirm that the ``testbias`` application functions as expected by verify the input and
      output are equal when assigning a testbias of zero 0 (no change).

      ``% ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml``

      stdout of screen session::

         % cd /home/root/opencpi/applications/
         % export OCPI_LIBRARY_PATH=../artifacts/:../xilinx18_3_aarch64/artifacts/
         % export OCPI_DMA_CACHE_MODE=0
         % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0^C
         % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml
         Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
         Actual deployment is:
           Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in ../xilinx18_3_aarch64/artifacts//ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Mon Dec 13 19:04:08 2021
           Instance  1 bias (spec ocpi.core.bias) on hdl container 0: PL:0, using bias_vhdl/a/bias_vhdl in ../artifacts//testbias_z3u_base.bitz dated Mon Dec 13 19:04:08 2021
           Instance  2 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in ../xilinx18_3_aarch64/artifacts//ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Mon Dec 13 19:04:08 2021
         Application XML parsed and deployments (containers and artifacts) chosen [0 s 40 ms]
         Application established: containers, workers, connections all created [0 s 66 ms]
         Dump of all initial property values:
         Property   0: file_read.fileName = "test.input" (cached)
         Property   1: file_read.messagesInFile = "false" (cached)
         Property   2: file_read.opcode = "0x0" (cached)
         Property   3: file_read.messageSize = "0x10"
         Property   4: file_read.granularity = "0x4" (cached)
         Property   5: file_read.repeat = "false"
         Property   6: file_read.bytesRead = "0x0"
         Property   7: file_read.messagesWritten = "0x0"
         Property   8: file_read.suppressEOF = "false"
         Property   9: file_read.badMessage = "false"
         Property  16: bias.biasValue = "0x0" (cached)
         Property  20: bias.test64 = "0x0"
         Property  31: file_write.fileName = "test.output" (cached)
         Property  32: file_write.messagesInFile = "false" (cached)
         Property  33: file_write.bytesWritten = "0x0"
         Property  34: file_write.messagesWritten = "0x0"
         Property  35: file_write.stopOnEOF = "true" (cached)
         Property  39: file_write.suppressWrites = "false"
         Property  40: file_write.countData = "false"
         Property  41: file_write.bytesPerSecond = "0x0"
         Application started/running [0 s 1 ms]
         Waiting for application to finish (no time limit)
         Application finished [0 s 20 ms]
         Dump of all final property values:
         Property   0: file_read.fileName = "test.input" (cached)
         Property   1: file_read.messagesInFile = "false" (cached)
         Property   2: file_read.opcode = "0x0" (cached)
         Property   3: file_read.messageSize = "0x10"
         Property   4: file_read.granularity = "0x4" (cached)
         Property   5: file_read.repeat = "false" (cached)
         Property   6: file_read.bytesRead = "0xfa0"
         Property   7: file_read.messagesWritten = "0xfa"
         Property   8: file_read.suppressEOF = "false" (cached)
         Property   9: file_read.badMessage = "false"
         Property  16: bias.biasValue = "0x0" (cached)
         Property  20: bias.test64 = "0x0" (cached)
         Property  31: file_write.fileName = "test.output" (cached)
         Property  32: file_write.messagesInFile = "false" (cached)
         Property  33: file_write.bytesWritten = "0xfa0"
         Property  34: file_write.messagesWritten = "0xfb"
         Property  35: file_write.stopOnEOF = "true" (cached)
         Property  39: file_write.suppressWrites = "false" (cached)
         Property  40: file_write.countData = "false" (cached)
         Property  41: file_write.bytesPerSecond = "0x44229"

      ..

#. Verify that the data has successfully transferred through the application by performing an
   m5sum on the input and output data files with bias effectively disabled, by setting the
   biasValue=0.

   Compare the md5sum of both ``test.input`` and ``test.output``. The stdout should be as follows:

   ::

      % md5sum test.*
      2934e1a7ae11b11b88c9b0e520efd978  test.input
      2934e1a7ae11b11b88c9b0e520efd978  test.output

   ..

.. note::

   **This shows that with a biasvalue=0 (no change in data) that the input matches the output
   and the testbias application is working as it should.**

..


.. _dev-HDL-DP-Verification-FSK-application-filerw-mode-label:

HDL DP Verification: FSK application filerw mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- (OPTIONAL) Execute another standard application which relies upon the HDL Data Plane

**IMPLEMENTATION:**

.. note::

   **The** :ref:`dev-Component-Unit-Test-results-table-label` **section in the
   appendix contains the verification test results of the Z3u board.**

..

This section is outlined in the following location:

   ``/home/user/opencpi/projects/assets/application/FSK/doc/FSK_app.tex``.

This document covers the FSK ``txrx`` execution mode however, the ``filerw`` execution mode will be
used in this section to validate the implementation of the Data Plane. To retrieve the contents
of the ``FSK_app.tex`` file install **rubber** and run the following command:

``$ rubber -d FSK_App_Getting_Started_Guide.tex``

``$ envince FSK_App_Getting_Started_Guide.pdf``

The following is a step-by-step set of instructions for executing the FSK application in ``filerw``
mode:

.. note::

   #. **$ indicates the command is performed on the Development Host**

   #. **% indicates the command is performed on the Target-platform**

..

#. **Build the FSK Application executable and copy it into the microSD card**

   ``$ cd /home/user/opencpi/projects/assets/applications/FSK``

   ``$ ocpidev build --rcc-platform xilinx18_3_aarch64``

   ::

      Xilinx RCC platform is: xilinx18_3_aarch64. Version is: 18_3. Architecture is: aarch64
      Compiling source file: FSK.cxx for platform xilinx18_3_aarch64
      Creating executable for "FSK" running on platform xilinx18_3_aarch64 from target-xilinx18_3_aarch64/FSK.o
      ./scripts/gen_rrcos_taps.py 128 0.95 `echo "1/64000" | bc -l` `echo "64000*39" | bc -l` 4096 idata/tx_rrcos_taps.dat
      ('\n', '********************************************************************************')
      *** Python: Generate Root-Raised Cosine taps ***
      1.2588365809784572
      [  85   96  105  111  115  116  114  109  100   88   72   53   31    6
      -21  -50  -81 -113 -145 -177 -207 -235 -259 -280 -295 -305 -307 -302
      -287 -263 -229 -184 -128  -60   20  112  217  334  462  602  752  912
      1081 1257 1441 1629 1821 2015 2209 2402 2593 2779 2958 3130 3292 3443
      3581 3705 3814 3906 3981 4038 4076 4096 4096 4076 4038 3981 3906 3814
      3705 3581 3443 3292 3130 2958 2779 2593 2402 2209 2015 1821 1629 1441
      1257 1081  912  752  602  462  334  217  112   20  -60 -128 -184 -229
      -263 -287 -302 -307 -305 -295 -280 -259 -235 -207 -177 -145 -113  -81
      -50  -21    6   31   53   72   88  100  109  114  116  115  111  105
      96   85]
      127862
      31.21630859375
      ./scripts/gen_rrcos_taps.py 128 0.95 `echo "1/64000" | bc -l` `echo "64000*39" | bc -l` 4096 idata/rx_rrcos_taps.dat
      ('\n', '********************************************************************************')
      *** Python: Generate Root-Raised Cosine taps ***
      1.2588365809784572
      [  85   96  105  111  115  116  114  109  100   88   72   53   31    6
      -21  -50  -81 -113 -145 -177 -207 -235 -259 -280 -295 -305 -307 -302
      -287 -263 -229 -184 -128  -60   20  112  217  334  462  602  752  912
      1081 1257 1441 1629 1821 2015 2209 2402 2593 2779 2958 3130 3292 3443
      3581 3705 3814 3906 3981 4038 4076 4096 4096 4076 4038 3981 3906 3814
      3705 3581 3443 3292 3130 2958 2779 2593 2402 2209 2015 1821 1629 1441
      1257 1081  912  752  602  462  334  217  112   20  -60 -128 -184 -229
      -263 -287 -302 -307 -305 -295 -280 -259 -235 -207 -177 -145 -113  -81
      -50  -21    6   31   53   72   88  100  109  114  116  115  111  105
      96   85]
      127862
      31.21630859375

   ``$ cd /home/user/opencpi/projects/assets/applications``

   ``$ cp -rf FSK/ /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64/opencpi/applications``

#. **Build the fsk_filerw HDL Container (i.e. bitstream) and copy it into the microSD card**

   ``$ cd /home/user/opencpi/projects/assets/hdl/assemblies/fsk_filerw``

   ``$ ocpidev build --hdl-platform z3u``

   ``$ cd container-fsk_filerw_z3u_base/target-zynq_ultra``

   ``$ cp fsk_filerw_z3u_base.bitz /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64/opencpi/artifacts``

#. Implement missing .so files into the microSD card xilinx18_3_aarch64 artifacts directory

   .. note::

      The following files are needed based off of the **/home/user/opencpi/projects/assets/
      applications/FSK/app_fsk_filerw.xml** file under RCC Components. The ocpi.core.file_read is
      already given but the **ocpi.assets.dsp_comps.baudTracking** and **ocpi.assets.dsp_comps.real_
      digitizer** components need to be supplemented. Check the **/home/user/opencpi/cdk/z3u
      /sdcard-xilinx18_3_aarch64/opencpi/xilinx18_3_aarch64/artifacts** to see what is included
   ..

   #. Copy the Baudtracking_simple.so into the microSD card xilinx18_3_aarch64 artifacts directory

      ::

         $ cd /home/user/opencpi/projects/assets/components/dsp_comps/Baudtracking_simple.rcc/target-xilinx18_3_aarch64

      ..

      ::

         $ cp Baudtracking_simple.so /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64/opencpi/xilinx18_3_aarch64/artifacts/

      ..

   #. Copy the real_digitizer.so into the microSD card xilinx18_3_aarch64 artifacts directory

      ::

         $ cd /home/user/opencpi/projects/assets/components/dsp_comps/real_digitizer.rcc/target-xilinx18_3_aarch64

      ..

      ::

         $ cp real_digitizer.so /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64/opencpi/xilinx18_3_aarch64/artifacts/

      ..

#. Copy the newly implemented file onto the microSD card

   ::

      $ cp -RLp /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64/opencpi/ /run/media/<user>/root/home/root/

   ..

#. Execute the :ref:`dev-Boot-Z3u-label` section

#. Execute the :ref:`dev-Standalone-mode-setup-label` section

#. Setup the library path to the artifacts:

   ::

      % cd /home/root/opencpi/applications/FSK
      % export OCPI_LIBRARY_PATH=/home/root/opencpi/artifacts:/home/root/opencpi/xilinx18_3_aarch64/artifacts
      % export OCPI_DMA_CACHE_MODE=0

   ..

#. Run application: FSK filerw:

   ``% ./target-xilinx18_3_aarch64/FSK filerw``

   ::

      % ./target-xilinx18_3_aarch64/FSK filerw
      Application properties are found in XML file: app_fsk_filerw.xml
      App initialized.
      App started.
      Waiting for done signal from file_write.
      real_digitizer: sync pattern 0xFACE found
      App stopped/finished.
      Bytes to file : 8950
      TX FIR Real Peak       = 4696
      Phase to Amp Magnitude = 20000
      RP Cordic Magnitude    = 19481
      RX FIR Real Peak       = 13701
      Application complete

   ..

#. In order to confirm that the output file is correct, it must be copied back to the development
   host so that the graphical display utility can be executed.

   - Copy/move the ``/home/root/opencpi/applications /FSK/odata/out_app_fsk_filerw.bin`` to the
     development host.

#. View the results of the output file

   ``$ eog \<path>/out_app_fsk_filerw.bin``

.. figure:: figures/oriole.png
   :alt: Oriole
   :align: center

   Oriole

..


.. _dev-Component-Unit-Testing-label:

Component Unit Testing
----------------------

**GOAL:**

- To build, run and capture the results for each of the Component Unit Tests provided with the
  OpenCPI framework. The ``PASS`` (P) or ``FAIL`` (F), results for each test are captured in the
  :ref:`dev-Component-Unit-Test-results-table-label` section. The table compares the ``PASS and FAIL``
  results to the FOSS supported zcu104. Refer to this table to ensure that testing behavior is
  consistent.


.. _dev-Build-the-Unit-Tests-(in-parallel)-on-the-Development-Host-label:

Build the Unit Tests (in parallel) on the Development Host
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   This process can be done in parallel. Open three different terminals and build all three Unit
   Test project directories (core, assets, assets_ts).

..

#. Build the component unit tests from project: **core** (Takes several hours)

   Open a new terminal on your local machine

   ``$ cd /home/user/opencpi``

   ``$ source cdk/opencpi-setup.sh -s``

   ``$ cd /home/user/opencpi/projects/core/components``

   ``$ ocpidev build test --hdl-platform z3u``

#. Build the component unit tests from project: **assets** (Takes several hours)

   Open a new terminal on your local machine

   ``$ cd /home/user/opencpi``

   ``$ source cdk/opencpi-setup.sh -s``

   ``$ cd /home/user/opencpi/projects/assets/components/<sub-directory>/``

   ``$ ocpidev build test --hdl-platform z3u``

#. Build the component unit tests from project: **assets_ts** (Takes several hours)

   Open a new terminal on your local machine

   ``$ cd /home/user/opencpi``

   ``$ source cdk/opencpi-setup.sh -s``

   ``$ cd /home/user/opencpi/projects/assets_ts/components/``

   ``$ ocpidev build test --hdl-platform z3u``


.. _dev-Run-the-Unit-Tests-(Sequentially)-on-the-Development-Host-label:

Run the Unit Tests (Sequentially) on the Development Host
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   #. **This section is very painstaking. In order to be certain, that each Component Unit
      Test is performed, the user must traverse into each unit *\.test directory of each
      component library and to execute the unit test.**

      - There alternative methods, but this is the most thorough to execute.

   #. **Some of these tests are known to fail or partically fail, per the their performance
      on a FOSS supported OSP (i.e. zcu104).**

   #. **A chart is provided in the** :ref:`dev-Component-Unit-Test-results-table-label` **section below
      that outlines the expected outcome for each of these tests (as of v2.2.0).**

#. Setup for :ref:`dev-Server-Mode-setup-label`

#. Run the component unit tests from project: **core**

   ``$ cd /home/user/opencpi/projects/core/components/<>.test``

   ``$ ocpidev run --only-platform z3u --accumulate-errors``

#. Run the component unit tests from project: **assets**

   ``$ cd /home/user/opencpi/projects/assets/components/<sub-directory>/<>.test``

   ``$ ocpidev run --only-platform z3u --accumulate-errors``

#. Run the component unit tests from project: **assets_ts**

   ``$ cd /home/user/opencpi/projects/assets_ts/components/<>.test``

   ``$ ocpidev run --only-platform z3u --accumulate-errors``


.. _dev-Enable-support-for-AD9361-label:

Enable support for AD9361
-------------------------

**BACKGROUND:**

- While the ``AD9361`` IC is a dual channel transceiver, the ``Z3u`` supports one RX only
  and a Tx/RX interfaces.

**GOAL:**

- Add Device Workers to the ``Platform Worker`` that are required to support the ``AD9361``.

- Build test assembly(ies) to prove that the interfaces to the ``AD9361`` are functional.

**IMPLEMENTATION:**

- Declare the Device Workers that support the ``AD9361`` to the ``Z3u`` Platform Worker

- Create/Update a XDC constraints file

- Create HDL Assemblies and Containers XML files for testing the 1Tx/1Rx configuration

   - i.e. ``fsk_modem``

- Create HDL Assemblies and Containers XML files for testing the dual Rx configuration

   .. warning::

      A bug in FOSS version 2.2.0 does not allow for dual Rx applications to execute properly

   ..

- Run the applications to verify the above configurations and verifying the output file

   - 1Tx/1Rx - fsk_modem_app

   - Dual RX - **Not supported by FOSS v2.2.0**

.. note::

   **CODEBLOCK: The ``finalized`` code block of various files mentioned in this section are located
   in the ocpi.osp.epiq_solutions repository and whose contents should be referenced for the
   remainder of this section.**

   ::

      ocpi.osp.epiq_solutions/hdl/platforms/z3u/

      ocpi.osp.epiq_solutions/hdl/assemblies/fsk_modem/

      ocpi.osp.epiq_solutions/hdl/applications/fsk_dig_radio_ctrlr/

   ..

..


.. _dev-Update-HDL-Platform-Worker-to-declare-HDL-Device-Workers-label:

Update HDL Platform Worker to declare HDL Device Workers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- Since the ``Z3u`` does not implement a slot/card paradigm for the ``AD9361``, then **ALL** HDL
  Device Workers that are required in support of the the Z3u **MUST** be declared in the HDL
  ``Platform Worker``. **THEREFORE**, the data streaming protocol for the Z3u OSP is *limited* to
  the protocol of the HDL Device Workers declared in the ``Platform Worker``. **Furthermore**, if
  another protocol is desired, then protocol adapters must be implemented in the HDL Assembly.

**IMPLEMENTATION:**

#. Since the ``ocpi.ocp.e3xx`` supports the ``AD9361`` in a similar manner as the ``z3u``, it will
   be referenced in this section.

#. Consolidated the appropriate constraints files ``z3u.xdc`` and ``sidekiq_z3u.xdc`` to support
   the ``AD9361`` and the other various signals to support complete functionality of the ``Z3u``.

#. Create an HDL Assembly and Container XML in support of the ``fsk_modem``

#. Build the bitstream for the ``fsk_modem``

#. Create a DRC App Worker (``drc_z3u.rcc``), by using the ``assets/hdl/cards/drc_fmcomms_2_3.rcc``
   as a benchmark

#. Run ``fsk_modem_app`` (any OCPI ``setup mode`` is possible: Standalone, Network, Server)

.. _dev-Create-the-fsk_modem-HDL-Assembly-and-Container-XML-label:

Create the fsk_modem HDL Assembly and Container XML
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- For better organizational purposes, an HDL Assembly directory and XML are created, named
  ``fsk_modem`` and located in this ``projects/osps/ocpi.ocp.epiq_solutions``` repository. The
  HDL Assembly XML (fsk_modem.xml) from the ``assets/hdl/assemblies/fsk_modem/fsk_modem.xml``
  is simply copied into this directory.

- The HDL Container XML and constraints files are created so that the z3u is included within the
  build of the fsk_modem HDL Assembly.

- Additionally, the **e310** Container XML file for the fsk_modem is also referenced.

**IMPLEMENTATION:**

#. Clone the ``ocpi.osp.e3xx`` project

   ``$ cd ~/Downloads``

   ``$ git clone -b release-2.2.0 git@gitlab.com:opencpi/osp/ocpi.osp.e3xx.git``

#. Create an assembly library

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev create hdl assembly fsk_modem``

   ``$ cd hdl/assemblies/fsk_modem``

#. Copy and edit files from ``ocpi.osp.e3xx/hdl/assemblies/fsk_modem/cnt.xml``

   #. ``$ cp /home/user/Downloads/ocpi.osp.e3xx/hdl/assemblies/fsk_modem/cnt.xml ./cnt_1tx_1rx.xml``

   #. Edit:

      ::

         only='z3u'

         constraints='z3u.xdc'

         Remove all card='*' attributes

         Change interconnect='zynq' to 'zynq_ultra'

      ..

#. Create a ``Makefile`` to declare the Containers to build

   ::

      Containers=\
              cnt_1tx_1rx.xml

      DefaultContainers=
      ExcludePlatforms=isim modelsim xsim
      Libraries+=misc_prims util_prims dsp_prims comms_prims

   ..

#. Update the ``z3u.xdc`` by examining the following file:

   ::

      ocpi.osp.e3xx/hdl/assemblies/fsk_modem/e31x_mimo_xcvr_data_src_qadc_ad9361_sub_data_sink_qdac_ad9361_sub_mode_2_cmos.xdc

   ..

   Integrate the relevant ``AD9361`` constraints into the ``hdl/platforms/z3u/z3u.xdc`` file.


.. _dev-Build-the-fsk_modem-HDL-Assembly-and-Container-label:

Build the fsk_modem HDL Assembly and Container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Building the fsk_modem HDL Assembly and Containers (bitstreams)

   ``$ cd /home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions``

   ``$ ocpidev build --hdl-assembly fsk_modem --hdl-platform z3u``


.. _dev-Create-the-fsk-modem-Application-label:

Create the fsk_modem Application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- For better organizational purposes, a local copy of the ``assets/applications/fsk_dig_radio_ctrlr/``
  is created within the ``projects/osps/ocpi.ocp.epiq_solutions`` repository and modified
  modified as needed to support the ``Z3u``.

**IMPLEMENTATION:**

#. Make a local copy of the ``fsk_dig_radio_ctrlr`` application directory

   ``$ cd ocpi.osp.epiq_solutions``

   ``$ cp -rf ../../assets/applications/fsk_dig_radio_ctrlr/ applications/``

#. Edit the local ``fsk_modem_app.xml`` file to modify the target output filename to be more
   generic:

   From: ``fsk_dig_radio_ctrlr_fmcomms_2_3_txrx.bin``

   To: ``odata.bin``


.. _dev-Running-the-fsk_modem_app-label:

Running the fsk_modem_app
^^^^^^^^^^^^^^^^^^^^^^^^^

#. Setup z3u for :ref:`dev-Network-Mode-setup-label`

   - i.e. customize the ``mynetsetup.sh`` to NFS mount to the development host's cdk/ and the various
     projects containing necessary run-time artifacts

   - Be sure that the ``mynetsetup.sh`` script has the following mount points that will now include
     the ocpi.osp.epiq_solutions project::

        mkdir -p /mnt/net
        mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory
        mkdir -p /mnt/ocpi_core
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
        mkdir -p /mnt/ocpi_assets
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets
        mkdir -p /mnt/ocpi_assets_ts
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
        mkdir -p /mnt/ocpi_z3u
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions /mnt/ocpi_z3u

#. Change directories to the fsk_dig_radio_ctrlr application:

   ``% cd /mnt/ocpi_z3u/applications/fsk_dig_radio_ctrlr``

#. Disable the OCPI_DMA_CACHE_MODE which have shown issues with the Zynq UltraScale+ family

   .. warning::

      Required for FOSS versions pre-2.2.0
   ..

   ``% export OCPI_DMA_CACHE_MODE=0``

#. Setup artifacts search path

::

   % export OCPI_LIBRARY_PATH=/mnt/ocpi_z3u/hdl/assemblies/fsk_modem:/mnt/ocpi_z3u/hdl/devices:/mnt/ocpi_core/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so:/mnt/ocpi_core/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so:/mnt/ocpi_assets/artifacts/ocpi.assets.dsp_comps.Baudtracking_simple.rcc.0.xilinx18_3_aarch64.so:/mnt/ocpi_assets/artifacts/ocpi.assets.dsp_comps.real_digitizer.rcc.0.xilinx18_3_aarch64.so

..

#. Run app for enough time, -t, so that the entire data file (picture) can flow through the app

   .. note::

      **Intermittent issues have been seen when using the Network Mode setup to run the
      fsk_modem_app.xml, if the picture below does not present itself, perform ``ocpihdl unload``
      to ``unload`` the FPGA on the z3u. Once unloaded, rerun the app (which will ``load`` the
      FPGA with registers in a reset/initial state) and this should resolve the issue.**

   ..

   ``% ocpirun -v -d -x -t 15 fsk_modem_app.xml``

#. On your development host:

   - With the output file accessible or copied onto the development host:

   ``$ cd <path>/odata.bin``

   ``$ eog odata.bin``

   - **SUCCESS** is defined as the expected picture being displayed in its entirety


.. _dev-APPENDIX-label:

APPENDIX
--------

.. _dev-Bug-Fix-to-the-framework-label:

Bug Fixes to the framework
^^^^^^^^^^^^^^^^^^^^^^^^^^

In support of building and installing the Z3u, the following bug fixes are required **PRIOR** to
building the Matchstiq Z3u HDL Platform Worker and FSK assembly, respectively.

#.

   ::

      $ git diff /home/user/opencpi/tools/include/hdl/hdl-targets.mk
      WARNING: terminal is not fully functional
      -  (press RETURN)
      diff --git a/tools/include/hdl/hdl-targets.mk b/tools/include/hdl/hdl-targets.mk
      index 6815474..c2f7d9a 100644
      --- a/tools/include/hdl/hdl-targets.mk
      +++ b/tools/include/hdl/hdl-targets.mk
      @@ -83,7 +83,7 @@ HdlTargets_zynq_ise:=$(foreach tgt,$(HdlTargets_zynq),$(tgt)_ise_alias)
       # ev: quad core, mali gpu, H.264
       # eg: quad core, mali gpu
       # cg: dual core
      -HdlTargets_zynq_ultra:=xczu28dr xczu9eg xczu7ev xczu3cg
      +HdlTargets_zynq_ultra:=xczu28dr xczu9eg xczu7ev xczu3cg xczu3eg
       # Zynq UltraScale+ chips require full part to be specified
       # The default is based on the zcu104 dev board, which is the cheapest, and supported by webpack.
       HdlDefaultTarget_zynq_ultra:=xczu3cg-2-sbva484e

   ..

#.

   ::

      $ git diff /home/user/opencpi/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml
      WARNING: terminal is not fully functional
      -  (press RETURN)
      diff --git a/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml b/projects/assets/components/dsp_comps/index b872e84..4d644d0 100644
      --- a/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml
      +++ b/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml
      @@ -1,8 +1,8 @@
      -<HdlWorker Language="vhdl" Spec="dc_offset_filter-spec.xml" Version="2" DataWidth="32"
      -          ExactParts='zynq:xc7z020-1-clg484
      +<HdlWorker Language="vhdl" Spec="dc_offset_filter-spec.xml" Version="2" DataWidth="32">
      +<!--      ExactParts='zynq:xc7z020-1-clg484
                             zynq_ise:xc7z020_ise_alias-1-clg484
                             virtex6:xc6vlx240t-1-ff1156
      -                      stratix4:ep4sgx230k-c2-f40'>
      +                      stratix4:ep4sgx230k-c2-f40'-->

         <Property Name="LATENCY_p" Type="uchar" Parameter="true" Default="1"
                  Description='Number of clock cycles between a valid input and a valid output'/>

   ..


.. _dev-Build-Vendor-Reference-Design-Bitstream-label:

Build Vendor Reference Design Bitstream
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- The configuration of the ``PS core IP`` used on the ``Z3u`` has specific interfaces enabled and
  configured to support proper operation with devices that are connected to the Zynq UltraScale+
  MPSoC, such as DDR, SPI, I2C and UART. The goal here is to build the vendor reference design
  to ensure that there are no build errors, then make a copy of the project to begin enabling
  the OpenCPI ``Control Plane`` for the ``Z3u``.

**IMPLEMENTATION:**

#. Build per the vendor's documented instructions

   .. note::

      You may have already performed this step in the :ref:`dev-Review-Vendor's-Reference-Design-Package-label`
      section but is covered again here for convenience.

   ..

   - This step confirms that the design builds without errors

   #. Download vendor reference design, ``matchstiq_z3u_pdk_v3_15_1.tar.gz``

   #. ``$ mkdir vendor_reference_design_package``

   #. ``$ tar xzf matchstiq_z3u_pdk_v3_15_1.tar.gz -C vendor_reference_design_package``

   #. ``$ cd vendor_reference_design_package``

   #. ``$ unzip hdl-b4335123a3d21dadcf3d04ad09411d1d2954ebd7.zip``

   #. ``$ source /opt/Xilinx/Vivado/2018.3/settings64.sh``

   #. ``$ vivado -mode batch -source vivado_build.tcl``

#. Build per Vivado GUI

   - This step confirms that the design builds without errors when building via the GUI and creates
     a project file and block design necessary for subsequenct steps

   #. Download vendor reference design, ``matchstiq_z3u_pdk_v3_15_1.tar.gz`` (If not already
      downloaded)

   #. ``$ mkdir vendor_reference_design_package_gui``

   #. ``$ tar xzf matchstiq_z3u_pdk_v3_15_1.tar.gz -C vendor_reference_design_package_gui``

   #. ``$ cd vendor_reference_design_package_gui``

   #. ``$ unzip hdl-b4335123a3d21dadcf3d04ad09411d1d2954ebd7.zip``

   #. ``$ source /opt/Xilinx/Vivado/2018.3/settings64.sh``

   #. ``$ vivado &``

   #. In the Tcl Console: ``source vivado_build.tcl``

   #. Once the build it complete, review the Tcl Console to ensure no ERRORS are present

#. Make a copy of the design and rebuild

   #. File -> Project -> Save As

      - Change ``Project Location``: ``vendor_reference_design_package_gui_saved``

      - Set ``Project name``: ``sidekiq_z3u_pdk``

      - Check box for ``Import all files to the new project``

      - Click ``OK``

   #. When ``Critical Messages`` pop-up appears, click ``OK``.

   #. Confirm in the top most banner, that the newly saved project is opened.

   #. In the yellow banner ``Implmented Design is out-of-date``... , click ``Close Design``

   #. In the ``Design Modified on Disk`` pop-up, click ``Don't Save``. The ERRORs in the Tcl Console
      can be ignored.

   #. The newly saved project ``vendor_reference_design_package_gui_saved``, should now be open

   #. Rebuild this project, by ``Generate Bitstream``

   #. Once finished, click ``OK`` to open the implementation

   #. When ``Critical Messages`` pop-up appears (Timing failure), click ``OK``

   #. Review the Tcl Console to any major ERRORS (take note of them)

   #. Close Vivado (it will be reopen in another section for editing)

.. _dev-Standalone-Mode-setup-label:

Standalone Mode setup
^^^^^^^^^^^^^^^^^^^^^

**GOAL**

- The goal of this section is to enable the user with with ability to setup the Standalone Mode
  on the Z3u. Success of this section is the ability to source the ``mysetup.sh`` script that
  enables  the Standalone Mode and provides the ability for the Platform Host (Z3u) to load
  the local bitstream.

**IMPLEMENTATION**

#. If not already done, execute the :ref:`dev-Boot-Z3u-label` section.

#. ``# export OCPI_LOCAL_DIR=/home/root/opencpi``

#. ``# cd /home/root/opencpi/``

#. ``# cp default_mysetup.sh ./mysetup.sh``

#. Edit the ``mysetup.sh`` script to target the local bitstream that you want to use.
   (default ``testbias``)

#. ``# source /home/root/opencpi/mysetup.sh``

   ::

      root@z3u:~/opencpi# source /home/root/opencpi/mysetup.sh
      Attempting to set time from time.nist.gov
      rdate: bad address 'time.nist.gov'
      ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
      Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
      Running login script.
      OCPI_CDK_DIR is now /home/root/opencpi
      OCPI_ROOT_DIR is now /home/root/opencpi/..
      Executing /home/root/.profile.
      No reserved DMA memory found on the linux boot command line.
      [  122.605311] opencpi: loading out-of-tree module taints kernel.
      [  122.612708] opencpi: dma_set_coherent_mask failed for device ffffffc06d22c400
      [  122.619921] opencpi: get_dma_memory failed in opencpi_init, trying fallback
      [  122.626940] NET: Registered protocol family 12
      Driver loaded successfully.
      OpenCPI ready for zynq.
      Loading bitstream
      Bitstream loaded successfully
      Discovering available containers...
      Available containers:
       #  Model Platform            OS     OS-Version  Arch     Name
       0  hdl   z3u                                             PL:0
       1  rcc   xilinx18_3_aarch64  linux  18_3        aarch64  rcc0

   ..

.. _dev-Network-Mode-setup-label:

Network Mode setup
^^^^^^^^^^^^^^^^^^

**GOAL**

- The goal of this section is to enable the user with the ability to setup the Network Mode
  on the Z3u. Success of this section is the ability to source the customized 'mynetsetup.sh"
  script that enables the Network Mode and provided the ability to load bitstreams from the
  Development Host (Computer) to the Platform Host (Z3u).

**IMPLEMENTATION**

#. Configure the Development Host's /etc/exports.d/user.exports to allow the NFS mounting
   of the framework and it's projects, including the osp/, for example. Restart the
   NFS server.

   /home/user/opencpi 192.168.0.0/16(rw,sync,no_root_squash,crossmnt,fsid=38)

#. Establish a serial connection from the Host to the Z3u, open a terminal window:

   ``$ sudo screen /dev/ttyUSB0 115200``

   ::

      PetaLinux 2018.3 z3u /dev/ttyPS0
      z3u login:root
      Password:root
      root@z3u:~#

   ..

#. Mount the OpenCPI filesystem

   ``# mkdir /home/root/opencpi``

   ``# cd opencpi/``

#. Copy the mynetsetup.sh for editing

   ``# cp default_mynetsetup.sh ./mynetsetup.sh``

   ``# vi mynetsetup.sh``

#. Edit the ``mynetsetup.sh`` per the configuration of the development host to access its
   ``opencpi/``.
   Under the section ``# Mount the opencpi development system as an NFS server, onto /mnt/net ...``.
   add the following lines which are necessary for mount the core, platform, assets, and assets_ts
   FOSS built-in projects.

   ::

        mkdir -p /mnt/net
        mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory
        mkdir -p /mnt/ocpi_core
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
        mkdir -p /mnt/ocpi_platform
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/platform /mnt/ocpi_platform
        mkdir -p /mnt/ocpi_assets
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets
        mkdir -p /mnt/ocpi_assets_ts
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
   ..

#. Implement edits to automatically setup IP-Address

   .. note::

      NOT APPLICABLE!
      At boot, the Z3u automatically configures its IP address to 192.168.0.15

   ..

#. Source the ``mynetsetup.sh`` script to enable OpenCPI Network Mode

   #. root@z3u~# ``cd opencpi/``

   #. root@z3u~/opencpi# ``export OCPI_LOCAL_DIR=/home/root/opencpi``

   #. root@z3u~/opencpi# ``source /home/root/opencpi/mynetsetup.sh <dev host IP address> /home/user/opencpi``

      - Example: ``source /home/root/opencpi/mynetsetup.sh 192.168.0.20 /home/user/opencpi``

    ::

       root@/home/root/opencpi# source /home/root/opencpi/mynetsetup.sh 192.168.0.20 /home/user/opencpi
       An IP address was detected.
       My IP address is: 192.168.0.15, and my hostname is: z3u
       Attempting to set time from time.nist.gov
       rdate: bad address 'time.nist.gov'
       ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
       Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
       Running login script.
       OCPI_CDK_DIR is now /mnt/net/cdk.
       OCPI_ROOT_DIR is now /mnt/net.
       Executing /home/root/.profile
       No reserved DMA memory found on the linux boot command line.
       [  480.135392] opencpi: loading out-of-tree module taints kernel.
       [  480.142325] opencpi: dma_set_coherent_mask failed for device ffffffc06cebcc00
       [  480.149535] opencpi: get_dma_memory failed in opencpi_init, trying fallback
       [  480.156544] NET: Registered protocol family 12
       Driver loaded successfully.
       OpenCPI ready for zynq.
       Loading bitstream
       Bitstream successfully loaded
       Discovering available containers...
       Available containers:
       #  Model Platform            OS     OS-Version  Arch     Name
       0  hdl   z3u                                             PL:0
       1  rcc   xilinx18_3_aarch64  linux  18_3        aarch64  rcc0
       %

   ..

    ::

       % pwd
       /home/root/opencpi
       %
       % env | grep OCPI
       OCPI_LOCAL_DIR=/home/root/opencpi
       OCPI_ROOT_DIR=/mnt/net
       OCPI_HDL_PLATFORM=
       OCPI_LIBRARY_PATH=/mnt/net/project-registry/ocpi.core/exports/artifacts:/mnt/net/cdk/xilinx18_3_aarch64/artifacts:/mnt/net/projects/assets/artifacts:/home/root/opencpi/xilinx18_3_aarch64/artifacts
       OCPI_TOOL_PLATFORM=xilinx18_3_aarch64
       OCPI_RELEASE=opencpi-v2.1.0
       OCPI_CDK_DIR=/mnt/net/cdk
       OCPI_TOOL_DIR=xilinx18_3_aarch64
       OCPI_SYSTEM_CONFIG=/home/root/opencpi/system.xml
       OCPI_DEFAULT_HDL_DEVICE=pl:0
       OCPI_ENABLE_HDL_SIMULATOR_DISCOVERY=0
       OCPI_TOOL_OS=linux
       %

   ..

.. _dev-Server-Mode-setup-label:

Server Mode setup
^^^^^^^^^^^^^^^^^

**GOAL:**

- To enable the user with the ability to setup the Server Mode, which is used extensively for
  performing Component Unit Testings.

- Success of this section is the ability to utilize the ocpiremote utility that enables the Server
  Mode and provides the ability to load bitstreams from the Client-side (Host) to the Server-side
  (embedded device).

**IMPLEMENTATION:**

Enable the target platform for remote containers by ensuring its system.xml contains
<remoteload='1'> and <socket load='1'>. The system.xml can be found and changed in one of two
places. Option 1 more robust than option 2, while option 2 is quicker to implement.

**Option 1:**

``/home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions/hdl/platforms/z3u/sd_card/system.xml``

   - If it does not match the code-block below, you will have to edit it and rebuild, reinstall
     and redeploy the platform.

**Option 2:**

``/home/user/opencpi/cdk/z3u/system.xml``

   - If it does not match the code-block below, you can edit it here and redeploy the platform.

   - However, if the hdl-platform is deleted and redeployed, this system.xml will not updated.

   ::

         <opencpi>
           <container>
             <rcc load='1'/>
             <remote load='1'/>
             <hdl load='1'>
             <device name='PL:0' platform='z3u'/>
             </hdl>
           </container>
           <transfer smbsize='128K'>
             <pio load='1' smbsize='10M'/>
             <dma load='1'/>
             <socket load='1'/>
           </transfer>
         </opencpi>

   ..

**Setup:**

#. Server-side setup:

   :ref:`dev-Boot-Z3u-label`

#. Client-side setup:

   #. Change into the OpenCPI directory:

      ``$ cd /home/user/opencpi``

   #. Source OpenCPI

      ``$ source cdk/opencpi-setup.sh -s``

   #. Setup the OpenCPI enviornment variables that support remote containers:

      ``$ export OCPI_SERVER_ADDRESSES=<Valid ip-address>:<Valid port>``

      - example: export OCPI_SERVER_ADDRESSES=192.168.0.15:12345

      ``$ export OCPI_SOCKET_INTERFACE=<Valid socket>``

      - example: export OCPI_SOCKET_INTERFACE=enp0s20f0u4c2

      - ``Valid socket`` is the name of the Ethernet interface of the development host which can
        communicate with the target platform

   #. Load ``sandbox`` onto the server:

      ``$ ocpiremote load -s xilinx18_3_aarch64 -w z3u``

      Example of stdout:

      ::

         $ ocpiremote load -s xilinx18_3_aarch64 -w z3u
         Preparing remote sandbox...
         Tue Jul 13 12:17:24 UTC 2021
         Creating server package...
         Sending server package...
         Server package sent successfully.
         Getting status (no server expected to be running):
         Executing remote configuration command: status
         No ocpiserve appears to be running: no pid file

      ..

   #. Start the Server-Mode and set an environment variable to work-around a known issue with
      v2.2.0 lease of the OpenCPI DMA driver:

      ``$ ocpiremote start -b -e OCPI_DMA_CACHE_MODE=0``

      Example of stdout:

      ::

         $ ocpiremote start -b -e OCPI_DMA_CACHE_MODE=0
         Executing remote configuration command: start -B
         Reloading kernel driver:
         Loading opencpi bitstream
         The driver module was successfully unloaded.
         No reserved DMA memory found on the linux boot command line.
         Driver loaded successfully.
         PATH=/home/root/sandbox/xilinx18_3_aarch64/bin:/home/root/sandbox/xilinx18_3_aarch64/sdk/bin:/usr/bin:/bin
         LD_LIBRARY_PATH=xilinx18_3_aarch64/sdk/lib
         VALGRIND_LIB=
         nohup ocpiserve -v -p 12345 > 20210713-121855.log
         Server (ocpiserve) started with pid: 2416.  Initial log is:
         Discovery options:  discoverable: 0, loopback: 0, onlyloopback: 0
         Container server at <ANY>:12345
         Available TCP server addresses are:
             On interface br0: 192.168.0.15:12345
         Artifacts stored/cached in the directory "artifacts", which will be retained on exit.
         Containers offered to clients are:
         0: PL:0, model: hdl, os: , osVersion: , platform: z3u
         1: rcc0, model: rcc, os: linux, osVersion: 18_3, platform: xilinx18_3_aarch64
         --- end of server startup log success above

      ..

      .. warning::

         If you are presented with the following error while attempting to run any of the
         Unit-Tests you may need to disable or reconfigure your firewall

         ::

            OCPI( 2:991.0828): Exception during application shutdown: error reading from container server "": EOF on socket read
            Exiting for exception: error reading from container server "": EOF on socket read

         ..

      ..

.. _dev-Execute-OpenCPI-on-"factory"-Z3u-OS-(RCC only)-label:

Execute OpenCPI on "factory" Z3u OS (RCC only)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**BACKGROUND:**

- Based on review of the vendor reference design package, it is possible that the OpenCPI
  run-time utilities and applications which are based solely on RCC Workers, will successfully
  operate on the ``factory`` Z3u.

**GOAL:**

- Construct the ``opencpi/`` deployment directory that contains the OCPI run-time utilities, RCC
  workers and applications (testbias.xml) artifacts for running on the Z3u. This is accomplished
  by leveraging the build artifacts from the install/deploy of the zcu104 with
  xilinx18_3_aarch64, which constructs the opencpi/ that contains the application XML files,
  scripts and .so files necessary for setup and execution on the embedded system.

- Copy the opencpi/ onto the Z3u, setup the embedded system for running OpenCPI utilities and
  applications.

- Verify the execution of OpenCPI run-time utilities on the ``factory`` Z3u

- Verify the execution of running an OpenCPI RCC **ONLY** application ``testbias``


.. _dev-Construct-the-opencpi/-directory-label:

Construct the opencpi/ directory
""""""""""""""""""""""""""""""""

#. OPTION #1 - LESS manual, but MORE time

   .. note::

      **This step DOES require that the zcu104 HDL Platform to have been built, which takes many
      hours to complete.**

   ..

   #. Complete the entire section of :ref:`dev-OpenCPI-Staging-label`

   #. ``$ cp -rL cdk/zcu104/sdcard-xilinx18_3_aarch64/opencpi/ sd_card_opencpi/opencpi``

   #. ``$ cd sd_card_opencpi/opencpi``

   #. Edit release

      - Change to ``opencpi-v2.2.0 xilinx18_3_aarch64 z3u``

   #. Copy and edit the Standalone Mode setup script

      - ``$ cp default_mysetup.sh mysetup.sh``

      - Edit by commenting out the section that loads the bitstream

   #. Edit zynq_setup.sh by commenting out

      - setting of the LD_LIBRARY_PATH

      - loading the driver

   #. Edit system.xml to include hdl... ``z3u``

      ::

         <hdl load='1'>
             <device name='PL:0' platform='z3u'/>
         </hdl>

      ..

#. OPTION #2 - MORE manual, but LESS time

   .. note::

      **This step DOES NOT require that the zcu104 HDL Platform to have been built, which takes
      many hours to complete.**

   ..

   #. Complete (Install: xilinx18_3_aarch64 (an RCC platform)) of :ref:`dev-OpenCPI-Staging-label`

   #. ``$ mkdir -p /home/user/sd_card_opencpi/opencpi``

   #. ``$ cp -rL cdk/applications/ sd_card_opencpi/opencpi``

   #. ``$ cp -rL cdk/runtime/opencpi-setup.sh sd_card_opencpi/opencpi/``

   #. ``$ cp -rL cdk/runtime/scripts/ sd_card_opencpi/opencpi/``

   #. ``$ cp -rL cdk/xilinx18_3_aarch64/ sd_card_opencpi/opencpi``

   #. ``$ rm -f sd_card_opencpi/opencpi/kernel-headers.tgz``

   #. ``$ cp -f platforms/zynq/{zynq_setup.sh, zynq_setup_common.sh} sd_card_opencpi/opencpi``

   #. ``$ cp -f platforms/zynq/default-mysetup.sh sd_card_opencpi/opencpi/mysetup.sh``

   #. Copy and edit

      ``$ cp platforms/zynq/zynq_system.xml sd_card_opencpi/opencpi/system.xml``

      sd_card_opencpi/opencpi/system.xml to include hdl... ``z3u``

      ::

         <hdl load='1'>
             <device name='PL:0' platform='z3u'/>
         </hdl>

      ..

   #. Copy and edit the Standalone Mode setup script

      - ``$ cp default_mysetup.sh mysetup.sh``

      - Edit by commenting out the section that loads the bitstream

   #. Edit zynq_setup.sh by commenting out

      - setting of the LD_LIBRARY_PATH

      - loading the driver

   #. Create and edit sd_card_opencpi/opencpi/release

      - Edit to contain ``opencpi-v2.2.0 xilinx18_3_aarch64 z3u``


.. _dev-Run-on-the-Z3u-label:

Run on the Z3u
""""""""""""""

#. Connect a microUSB/USB-A cable from the Z3u to Dev Host

#. Connect USB-C/USB-A cable from Z3u to Dev Host. This will power-on the Z3u.
p
#. From development host, open a screen session

   $  ``sudo screen /dev/ttyUSB0 115000``

#. After Z3u has successfully booted, from development host, verify the Ethernet connection:

   #. Confirm that the Ethernet port has been created on the Dev Host with IP 192.168.0.20

   #. Successfully execute ping of the Z3u (192.168.0.15)

#. From development host, secure copy the opencpi/ to the Z3u

   ``$ scp -r sd_card_opencpi/opencpi/ sidekiq@192.168.0.15:/tmp``

#. From screen session:

   #. ``# sudo su -``

   #. Create a /home/root directory

      ``# mkdir /home/root/``

   #. ``# mv /tmp/opencpi /home/root``

   #. ``# chown -R root:root /home/root/opencpi``

   #. ``# cd /home/root/opencpi``

   #. ``# export OCPI_LOCAL_DIR=/home/root/opencpi``

   #. ``# source /home/root/opencpi/mysetup.sh``

   ::

      root@z3u:/home/root/opencpi# export OCPI_LOCAL_DIR=/home/root/opencpi
      root@z3u:/home/root/opencpi# source /home/root/opencpi/mysetup.sh
      Attempting to set time from time.nist.gov
      -su: rdate: command not found
      ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
      Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
      Running login script.
      OCPI_CDK_DIR is now /home/root/opencpi.
      OCPI_ROOT_DIR is now /home/root/opencpi/...
      Executing /etc/profile.d/opencpi-persist.sh.
      OpenCPI ready for zynq.
      Discovering available containers...
      [  118.579632] Synchronous External Abort: synchronous external abort (0x92000010) at 0x0000007fb0ed7000
      OCPI( 2:606.0437): HDL Device 'PL:0' gets a bus error on probe:
      OCPI( 2:606.0438): In HDL Container driver, got Zynq search error: bus error on probe
      Available containers:
      #  Model Platform            OS     OS-Version  Arch     Name
      0  rcc   xilinx18_3_aarch64  linux  18_3        aarch64  rcc0

   ..

   ::

      % env | grep OCPI
      OCPI_LOCAL_DIR=/home/root/opencpi
      OCPI_ROOT_DIR=/home/root/opencpi/..
      OCPI_LIBRARY_PATH=/home/root/opencpi/xilinx18_3_aarch64/artifacts:/home/root/pencpi/artifacts
      OCPI_TOOL_PLATFORM=xilinx18_3_aarch64
      OCPI_RELEASE=opencpi-v2.2.0
      OCPI_CDK_DIR=/home/root/opencpi
      OCPI_TOOL_DIR=xilinx18_3_aarch64
      OCPI_SYSTEM_CONFIG=/home/root/opencpi/system.xml
      OCPI_DEFAULT_HDL_DEVICE=pl:0
      OCPI_TOOL_OS=linux

   ..

#. Verify the execution of OpenCPI run-time utilities on the ``factory`` Z3u, by simply executing
   the tool without any arguements

   #. ``$ ocpirun``

   #. ``$ ocpihdl``

#. Verify the execution of running an OpenCPI RCC **ONLY** application ``testbias"

   #. ``$ cd /home/root/opencpi/applications``

   #. ``$ ocpirun -v -d testbias.xml``

   #. ``$ hexdump -n 10 test.output``

   #. ``$ ocpirun -v -d -pbias=biasValue=0 testbias.xml``

   ::

      % ocpirun -v -d -pbias=biasValue=0 testbias.xml
      [  250.229343] Synchronous External Abort: synchronous external abort (0x92000010) at 0x0000007f90693000
      OCPI( 2:738.0086): HDL Device 'PL:0' gets a bus error on probe:
      OCPI( 2:738.0087): In HDL Container driver, got Zynq search error: bus error on probe
      Available containers are:  0: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
      Actual deployment is:
      Instance  0 file_read (spec ocpi.core.file_read) on rcc container 0: rcc0, using file_read in /home/root/opencpi/xil
      inx18_3_aarch64/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Mon Jul 12 14:19:00 2021
      Instance  1 bias (spec ocpi.core.bias) on rcc container 0: rcc0, using bias_cc in /home/root/opencpi/xilinx18_3_aarch64/artifacts/ocpi.core.bias_cc.rcc.0.xilinx18_3_aarch64.so dated Mon Jul 12 14:19:00 2021
      Instance  2 file_write (spec ocpi.core.file_write) on rcc container 0: rcc0, using file_write in /home/root/opencpi/xilinx18_3_aarch64/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Mon Jul 12 14:19:00 2021
      Application XML parsed and deployments (containers and artifacts) chosen [0 s 37 ms]
      Application established: containers, workers, connections all created [0 s 2 ms]
      Dump of all initial property values:
      Property   0: file_read.fileName = "test.input" (cached)
      Property   1: file_read.messagesInFile = "false" (cached)
      Property   2: file_read.opcode = "0" (cached)
      Property   3: file_read.messageSize = "16"
      Property   4: file_read.granularity = "4" (cached)
      Property   5: file_read.repeat = "false"
      Property   6: file_read.bytesRead = "0"
      Property   7: file_read.messagesWritten = "0"
      Property   8: file_read.suppressEOF = "false"
      Property   9: file_read.badMessage = "false"
      Property  16: bias.biasValue = "0" (cached)
      Property  20: bias.testws = "0"
      Property  21: bias.param1 = "5" (parameter)
      Property  22: bias.param2 = "6,7,8" (parameter)
      Property  28: file_write.fileName = "test.output" (cached)
      Property  29: file_write.messagesInFile = "false" (cached)
      Property  30: file_write.bytesWritten = "0"
      Property  31: file_write.messagesWritten = "0"
      Property  32: file_write.stopOnEOF = "true" (cached)
      Property  36: file_write.suppressWrites = "false"
      Property  37: file_write.countData = "false"
      Property  38: file_write.bytesPerSecond = "0"
      Application started/running [0 s 1 ms]
      Waiting for application to finish (no time limit)
      Application finished [0 s 10 ms]
      Dump of all final property values:
      Property   0: file_read.fileName = "test.input" (cached)
      Property   1: file_read.messagesInFile = "false" (cached)
      Property   2: file_read.opcode = "0" (cached)
      Property   3: file_read.messageSize = "16"
      Property   4: file_read.granularity = "4" (cached)
      Property   5: file_read.repeat = "false" (cached)
      Property   6: file_read.bytesRead = "4000"
      Property   7: file_read.messagesWritten = "250"
      Property   8: file_read.suppressEOF = "false" (cached)
      Property   9: file_read.badMessage = "false"
      Property  16: bias.biasValue = "0" (cached)
      Property  20: bias.testws = "0" (cached)
      Property  28: file_write.fileName = "test.output" (cached)
      Property  29: file_write.messagesInFile = "false" (cached)
      Property  30: file_write.bytesWritten = "4000"
      Property  31: file_write.messagesWritten = "250"
      Property  32: file_write.stopOnEOF = "true" (cached)
      Property  36: file_write.suppressWrites = "false" (cached)
      Property  37: file_write.countData = "false" (cached)
      Property  38: file_write.bytesPerSecond = "563309"

   ..

#. md5sum test.input test.output

   ::

      2934e1a7ae11b11b88c9b0e520efd978  test.input
      2934e1a7ae11b11b88c9b0e520efd978  test.output

   ..


.. _dev-Clean-up-of-Standalone Mode-label:

Clean up of Standalone Mode
"""""""""""""""""""""""""""

- To clean up OpenCPI artifacts used for Standalone Mode testing and to ensure that there is no
  conflict with Network or Server Mode testing that is perform in later sections

#. ``$ rm -f /etc/profile.d/opencpi-presist.sh``

#. ``$ rm -rf /home/root/opencpi``


.. _dev-Program-FPGA-with-OpenCPI-artifact-using-Epiqs-prog_fpga-utility-label:

Program FPGA with OpenCPI artifact using Epiq's prog_fpga utility
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**GOAL:**

- If the OpenCPI run-time utilities have not yet been built, it is still possible to program the
  FPGA with an OpenCPI bitstream using Epiq's prog_fpga utility. Based on the review of Epiq's
  reference design, reprogramming is perform via device tree overlay. So, to employ this method,
  the OpenCPIbitstream file must be converted to a .bin file. The following steps are used to
  create the \*.bin file for leveraging Epiq's prog_fpga utility for reprogramming the FPGA.

**IMPLEMENTATION:**

#. ``$ cd ocpi.osp.epiq_solutions``

#. ``$ touch pattern_capture_asm_z3u_base.bit``

#. ``edit pattern_capture_asm_z3u_base.bit`` to include:

   ::

         all:
         {
                 pattern_capture_asm_z3u_base.bit /``*`` Bitstream file name ``*``/
         }

   ..

   #.

   ::

      $ cp /home/user/opencpi/projects/assets/hdl/assemblies/pattern_capture_asm
      /container-pattern_capture_asm_z3u_base/target-zynq_ultra/pattern_capture_asm_z3u_base.bit .

   ..

   #.

   ::

      $ /opt/Xilinx/SDK/2018.3/bin/bootgen -image pattern_capture_asm_z3u_base.bif -arch zynqmp-process_bitstream bin

   ..

#. Copy pattern_capture_asm_z3u_base.bit.bin onto Matchstiq Z3u

   ::

      $ scp /home/user/opencpi/projects/geon.osp.epiq_solutions /pattern_capture_asm_z3u_base.bit.bin sidekiq@192.168.0.15:/tmp

   ..

#. On the Matchstiq Z3u:

   #. Use Epiq utility to program FPGA with OpenCPI bitstream

      ``$ ./prog_fpga -c 0 -s /tmp/pattern_capture_asm_z3u_base.bit.bin``

   #. Used devmem/devmem2 to read the OpenCPI ``Magic Word``

      ::

         sidekiq@z3u: sudo su -
         root@z3u:/# devmem 0xa8000000
         0x4F70656E
         root@z3u:/# devmem 0xa8000004
         0x43504900

      ..


.. _dev-Test-results-tables-label:

Test results tables
^^^^^^^^^^^^^^^^^^^

- zcu104

  - **OpenCPI Version: v2.1.0**, Test Date:
  - **OpenCPI Version: v2.2.0**, Test Date:

- z3u

  - **OpenCPI Version: v2.1.0**, Test Date: 07/13/2021
  - **OpenCPI Version: v2.2.0**, Test Date: 01/18/2022

- Table Key:

  - **P**: *PASS*
  - **F** or **case##.##**: *FAIL*
  - **Excluded**: Test was *excluded* due to issues seen within the framework


.. _dev-Component-Unit-Test-results-table-label:

Component Unit Test results table
"""""""""""""""""""""""""""""""""

+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| FOSS Version                      | v2.1.0                | v2.1.0                | v2.2.0                | v2.2.0                |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| Platform Under Test               | zcu104                | z3u                   | zcu104                | z3u                   |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| Component Library Under Test      |                       |                       |                       |                       |
+===================================+=======================+=======================+=======================+=======================+
| **core/components**               |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| backpressure                      | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| bias                              | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| metadata_stressor                 | P                     | P                     | case00.00 -           | case00.00 -           |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       | case03.02             | case03.02             |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       |                       | Extra end bytes       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| **assets/components/comms_comps** |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| mfsk_mapper                       | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| **assets/components/dsp_comps**   |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cic_dec                           | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cic_int                           | case01.05,            | case01.05,            | ?,                    | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   | case01.10             | case01.10             |                       | Previous F excluded   |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| dc_offset_filter                  | P                     | P                     | P                     | P Makefile            |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       |                       | Change Req            |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| downsample_complex                | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| fir_complex_sse                   | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| fir_real_sse                      | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| iq_imbalance_fixer                | P                     | P                     | P                     | P Makefile            |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       |                       | Change Req            |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| phase_to_amp_cordic               | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| pr_cordic                         | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| rp_cordic                         | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| **assets/components/misc_comps**  |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cdc_bits_tester                   | Excluded              | Excluded              | Excluded              | Excluded              |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cdc_count_up_tester               | Excluded              | Excluded              | Excluded              | Excluded              |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cdc_fifo_tester                   | Excluded              | Excluded              | Excluded              | Excluded              |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cdc_pulse_tester                  | Excluded              | Excluded              | Excluded              | Excluded              |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cdc_single_bit_tester             | Excluded              | Excluded              | Excluded              | Excluded              |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| csts_to_iqstream                  | N/A                   | N/A                   | ?                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cswm_to_iqstream                  | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| data_src                          | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| delay                             | P                     | P                     | case00.00 -           | F                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       | case00.03             | Extra 4 bytes         |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| iqstream_to_csts                  | N/A                   | N/A                   | ?                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| iqstream_to_cswm                  | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| iqstream_to_timeiq                | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| timeiq_to_iqstream                | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| **assets/components/util_comps**  |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| agc_real                          | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| fifo                              | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| pattern_v2                        | P                     | P                     | P                     | case00.00,            |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       |                       | case01.00             |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| test_tx_event                     | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| timestamper                       | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| timestamper_scdcd                 | P                     | P                     | case00.00 -           | case00.00 -           |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       | case00.35             | case00.35             |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| timestamper_scdcd_csts            | N/A                   | N/A                   | ?                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| zero_pad                          | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| **assets_ts/components**          |                       |                       |                       |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| cic_dec_ts                        | P                     | P                     | case00.00 -           | F extra data captured |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       | case00.23             |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| complex_mixer_ts                  | Excluded              | Excluded              | Excluded              | Excluded              |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| dc_offset_filter_ts               | P                     | P                     | P                     | P                     |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| downsample_complex_ts             | P                     | P                     | case00.00 -           | F extra data captured |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       | case00.04             |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
| fir_complex_sse_ts                | P                     | P                     | case00.00 -           | F extra data captured |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+
|                                   |                       |                       | case00.02             |                       |
+-----------------------------------+-----------------------+-----------------------+-----------------------+-----------------------+


.. _dev-Application-verification-results-table-label:

Application verification results table
""""""""""""""""""""""""""""""""""""""

+-----------------------------------+---------------+---------------+---------------+---------------+
| FOSS Version                      | v2.1.0        | v2.1.0        | v2.2.0        | v2.2.0        |
+-----------------------------------+---------------+---------------+---------------+---------------+
| Platform Under Test               | zcu104        | z3u           | zcu104        | z3u           |
+-----------------------------------+---------------+---------------+---------------+---------------+
|                                   |               |               |               |               |
+-----------------------------------+---------------+---------------+---------------+---------------+
| Application Under Test            |               |               |               |               |
+===================================+===============+===============+===============+===============+
| pattern_capture_asm               | P             | P             | P             | P             |
+-----------------------------------+---------------+---------------+---------------+---------------+
| testbias                          | P             | P             | P             | P             |
+-----------------------------------+---------------+---------------+---------------+---------------+
| FSK/filerw                        | P             | P             | P             | P             |
+-----------------------------------+---------------+---------------+---------------+---------------+
| fsk_drc/fsk_modem_app             | P             | P             | P             | P             |
+-----------------------------------+---------------+---------------+---------------+---------------+
