.. Guide for developing Device / Proxy Workers for an i2c bus

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _Guide for developing Device/ Proxy Workers for i2c an bus:

Guide for developing Device / Proxy Workers for i2c an bus
=======================================================

Introduction
------------

OpenCPI FOSS Version: **v2.2.0**

This document provides an OpenCPI developer the instructions for developing ``Device Workers``,
``Device Proxies``, and a test ``Application`` for hardware devices that exist on a hardware
platform which are external to an FPGA.

Used as a ``Case Study`` throughout this guide, the I2C Bus #2 of the Epiq Solutions Matchstiq Z3u
(herein "Z3u") is referenced to help the developer understand each of the steps. The I2C Bus #2
hosts four devices: SiTime sit5356 and three Texas Instruments tmp103.

The full I2C Bus #2 implementation is available in the ``ocpi.osp.epiq_solutions`` Z3u OSP.

Upon completion of this guide, an OpenCPI developer should be equipped with the knowledge to implement
support for their own unique I2C bus.

Documentation
^^^^^^^^^^^^^

This document includes **some** limited definitions and framework direction but the developer
is encouraged to reference the OpenCPI guides for further information. Below is a list of guides
that are explicitly referenced.

  #. `OpenCPI Platform Development Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf>`_

  #. `OpenCPI User Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf>`_

  #. All Z3u ``Device Workers``, ``Device Proxies``, and test ``Applications`` contain an
     associated .rst datasheet that provide additional information.


Limitations
^^^^^^^^^^^

This section details all ``Limitations`` (documented FOSS ``Issues``) with the OpenCPI framework
that have been shown to impact the development of device workers and proxies in support of an I2C
bus implementation.

  #. `Issue 2662 <https://gitlab.com/opencpi/opencpi/-/issues/2662>`_ OCPI I2C does not support
     addresses greater than 8 bits

  #. `Issue 2663 <https://gitlab.com/opencpi/opencpi/-/issues/2663>`_ ushort byte-swapping read
     issue with I2C

  #. `Issue 2567 <https://gitlab.com/opencpi/opencpi/-/issues/2567>`_ NUSERS_p is ignored in the
     <device>-build.xml

Research
--------

During this stage of development, the developer must gather information about the external
devices' register map and connectivity. If the device is COTS, then the register map should be
available in a published datasheet. The connectivity of the device is normally only available
via schematics or sometimes with various guides that are provided by the manufacturer of the
platform.

In this case study of the I2C Bus #2, the I2C device addresses and connectivity are available
from the manufacturer's documentation.

If the device address, pin number, component datasheet, or register map are not easily
obtainable, then the developer may have to inquire about this information by other means such
as email, or manufacturer forum posts.

Platform Manufacturer Research
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At minimum, the following information must be identified:
   #. Manufacturer and part number for each of the device(s) present on the bus(es)
   #. I2C device address(es)
   #. Desired I2C clock rate
   #. FPGA Pin number(s) in support of the I2C bus or additional signals required for enabling
      access to all I2C devices

Below is a summary of the information collected for the I2C Bus #2:

.. list-table:: Devices and Addresses
   :widths: 25 25
   :header-rows: 1

   * - Device(s)
     - Address
   * - sit5356
     - 0x62
   * - tmp103
     - 0x70
   * - tmp103
     - 0x71
   * - tmp103
     - 0x72

``I2C Clock Rate``: **100 KHz**

.. list-table:: FPGA Pin numbers that support the I2C bus and access to all I2C devices
   :widths: 25 25
   :header-rows: 1

   * - Signal
     - FPGA pin #
   * - I2C Bus #2: SCL
     - R1
   * - I2C Bus #2: SDA
     - T1
   * - ref_int_mems
     - P5
   * - ref_ext_10m
     - R4
   * - ref_ext_40m
     - R5

Component Manufacturer Research
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At minimum, the following information must be identified:
   #. Device datasheet
   #. Device register map and accessibility (Read/Write, Read only, Write only)
   #. Default register values

Below is a summary of the information collected for the I2C Bus #2:

**Device Datasheets**:
   #. `sit5356 Datasheet <https://www.sitime.com/support/resource-library/datasheets/sit5356-datasheet>`_

   #. `tmp103 Datasheet <https://www.ti.com/lit/ds/symlink/tmp103.pdf?ts=1631117992048&ref_url=https%253A%252F%252Fwww.google.com%252F>`_

**Device Register Map**:
The device register map for each of these devices can be found within their respective device
datasheets. The register map for each device can also be reviewed in their respective -spec.xml
files.

   * **specs/sit5356-spec.xml**

   *  **specs/tmp103-spec.xml**

**Default register values**:
The default register values for each of these devices can be found within their respective device
datasheets.

Implementation
--------------

The developer leverages the information gathered in the ``Research`` phases to develop the
``Device Workers`` and ``Subdevice Worker``. The I2C ``Subdevice`` describes the make-up of the
I2C bus, by listing all of the ``Device Workers`` and their associated I2C device addresses and
provide a single point of entry for the Control Plane via the Raw Properties Port.

With the device workers and subdevice created, they must then be instanced in the ``Platform Worker``.
This allows the devices to be in-scope during the building of a bitstream.

The final step of implementation is created the ``Device Proxy``  which will perform developer
defined C++ functions based on the register-set and particular functionality of a specific device.

For brevity and readablity both devices (tmp103, sit5356) will not be covered in concise detail in
each section. Instead, for each particular section a device will be modeled based on its
'uniqueness' or 'difficulty' in the given section. This will enlighten the developer on the nuances
of OpenCPI where each of these sections are concerned.

Device Worker
^^^^^^^^^^^^^

**Devices** are hardware elements that are locally attached to the processor of the
platform. They are controlled by special workers called **device workers** (analogous to
"device drivers"), and usually act as sources or sinks for data into or out of the system, and thus
can be used for inputs and outputs for a component-based application running on that system.
[`OpenCPI Platform Development Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf>`_].

#. Define the Component Specification (OCS) for the external device based on its register map
   within the datasheet.

   * **specs/sit5356-spec.xml** is the OCS for the SiT5356.

      - Because this device has ushort (16-bit) data registers, the VHDL implementation must
        perform a 1 bit shift of the raw property address for proper data word alignment.

#. Create the Device Worker based on the OCS and edit the OpenCPI Worker Description file (OWD)
   and VHDL.

   * **sit5356.hdl/sit5356.xml** is the OWD for the Device Worker.

      - FirstRawProperty: Defines the first property that is to be accessed via the Raw Properties
        Port.

      - controlinterface Timeout: Defines the minimum number of control clock cycles that should be
        allowed for the worker to complete control operations or property access opertaions.

	- **CRITICAL NOTE: The means for accurately calculating the `controlinterface Timeout` value
	  is not adequately documented by FOSS.**

      - rawprops: A rawprop worker port consists of a VHDL record of signals that allow a device
        worker to easily delegate all of its raw property accesses to the subdevice.

      - frequency : SiT5356 devices are purchased with a fixed frequency. The ``frequency``
        property is unique to the Device Worker, but not Component, and allows said worker to be
        built for any frequency that the manufacturer supports. This property describes the
        hardware configuration in more detail and can be readback by a proxy or application and
        used to make decisions about the hardware capabilities for setup or reconfiguration.

   * **sit5356.hdl/sit5356.vhd** : The worker's HDL implementation file.

      - The VHDL connects the Control Plane Raw Properties to a Raw Properties Port which
        subsequently is connected to the I2C Subdevice Worker.

      - As mentioned in the OCS step, since the SiT5356 has 16 bit registers, the address
        (byte addressing) must be shifted by 1 bit.

         - For comparison, the tmp103 has only 8-bit data registers and therefore a 1 bit shift of
           the Raw Properties address is not required.

Subdevice Worker
^^^^^^^^^^^^^^^^

A **subdevice worker** implements the required sharing of low level hardware
between device workers. It is defined to support some number of device workers, and is thus
instantiated whenever any of its supported device workers are instantiated in a platform
configuration or container.
[`OpenCPI Platform Development Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf>`_].

Note: The **z3u_i2c-spec.xml** was utilized for all three I2C buses (0,1,2) of the Z3u.

#. Define the Component Specification (OCS)

   * **specs/z3u_i2c-spec.xml** is the OCS for the I2C bus(es) in support of the Matchstiq Z3u.

      - NUSERS_p: Property to define the number of Device Workers that require access to the I2C
        bus through the Subdevice.

         - A ``limitation/bug`` is outlined in Issue 2567. Due to the bug, the default value of
           NUSERS_p has to be set to the maximum number of devices that will be on any I2C bus that
           uses this spec.

      - SLAVE_ADDRESS_p: Property array that holds the bus addresses of each I2C device on the bus
        and is referenced when an access of the targeted I2C device is executed.

      - CLK_FREQ_p: Property Parameter that declares the clock rate of the Control Plane clock.
        During HDL build-time, this value is divided by the desired I2C clock rate, in this case
        100 KHz, to produce a clock cycle count value, which is a required input to the I2C
        primitive module.

#. Create the Subdevice Worker based on the OCS and edit the OpenCPI Worker Description file (OWD)
   and VHDL which is unique to the target I2C Bus #2 of the Z3u.

   * **z3u_i2c_bus2.hdl/z3u_i2c_bus2.xml** is the OWD for the Subdevice Worker.

      - Declares a Raw Properties Port for the number of expected Device Workers that it must
        support, the specific Device Workers (with an explicit index) that reside on the I2C bus
        and the I2C required signals.

      - rawprop: Defines how many rawprop signal bundles need to be generated (NUSERS_p = 4).

      - supports worker: Defines a worker that is supported by this subdevice.

      - port="rawprops": Defines that this worker is connected to the rawprops property.

      - index: Defines the index orientation of each worker that is supported by this subdevice.

         - **The ordering of these I2C addresses and the indexes defined in the
           z3u_i2c_bus2-build.xml MUST align with one another.**

      - Signal SDA/SCLK: Defines the two signal names that will be shared by the device workers
        that are defined on this subdevice. These signals will be routed to the appropriate pins in
        the platform worker.

   * **z3u_i2c_bus2.hdl/z3u_i2c_bus2-build.xml** describes the various build configurations of the
     Subdevice Worker.

      - SLAVE_ADDRESS_p: A Property Parameter which declares the I2C address(es) (in decimal) for
        each of the hardware I2C devices available on the I2C bus.

         - **The ordering of these I2C addresses and the indexes defined in the OWD MUST align with
           one another.**

   * **z3u_i2c_bus2.hdl/z3u_i2c_bus2.vhd**: OpenCPI HDL wrapper of an opensource I2C primitive
     module, which normalizes the I2C primitive to the Raw Properties Port.

     - Normally an I2C subdevice would be specialized for the platform and for the device it is
       multiplexing.

     - A ``limitation/bug`` is outlined in `Issue 2662 <https://gitlab.com/opencpi/opencpi/-/issues/2662>`_,
       OCPI I2C does not support addresses greater than 8 bits.

     - CLK_CNT: Defines the I2C Clock Rate on this I2C bus (100 KHz).

        - ``CLK_CNT => to_unsigned(from_float(CLK_FREQ_p)/100000,16) ) -- I2C clk = ~ 100kHz``

   **z3u_i2c_bus2.hdl/Makefile**: Additional build-time directives.

     - Libraries: Declare HDL primitive libraries that contain primitive modules used within the
       worker.

     - OnlyTargets: Ideally, this worker would only build for the Matchstiq-Z3U platform. However,
       this is blocked by AV-2044. Therefore, only build for the HDL target that represents the
       Matchstiq Z3u.

     - HdlExactPart: As this Subdevice Worker is specific to the Matchstiq Z3u, it is restricted to
       only be built for the FPGA that is available on said platform.

HDL Platform Worker
^^^^^^^^^^^^^^^^^^^

**HDL Platform Worker**: A specific type of HDL worker providing infrastructure for implementing
control/data interfaces to devices and interconnects external to the FPGA or simulator (e.g. PCI
Express, Clocks).
`OpenCPI Platform Development Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf>`_

For any Device/Subdevice Worker to be available for inclusion in a bitstream, it must be declared
in the Platform Worker OWD and its signals tied to FPGA pins.

#. Declare Device/Subdevice Workers in the Platform Worker OWD

   * **z3u.xml** is the Platform Worker OWD of the Matchstiq Z3u

      - **<device worker='z3u_i2c_bus2`...>**: Instantiates the z3u_i2c_bus2 subdevice worker.

      - **<property name='CLK_FREQ_q'/>**: Defines the expected frequency of the Control Plane
        for which the worker was built.

      - **<signal name: SDA / SCL platform='sda_2','scl_2'/>**: Ties the signal names that are
        shared by the device workers in the subdevice worker to the actual FPGA pins that are
        defined in the **z3u.xdc** constraints file. See the **z3u.xdc** section below for more
        detail.

      - **<device worker='sit5356'...>**: Instantiates the sit5356 device worker.

      - **<property name='frequency'/>**: Defines the frequency of the SiT5356 for which the
        hardware device was purchased.

      - Required for enabling the ability to access the SiT5356.

         - **<signal output="ref_int_mems"/>**

         - **<signal output="ref_ext_10m"/>**

         - **<signal output="ref_ext_40m"/>**

         - **<device worker='tmp103'...>**: Instantiates the tmp103 (x3) device workers.

#. Platform Worker HDL

   * MUST set these signals for enabling I2C access to the SiT5356

      - ref_int_mems <= '1';

      - ref_ext_10m  <= '0';

      - ref_ext_40m  <= '0';

HDL Assembly / Container
^^^^^^^^^^^^^^^^^^^^^^^^

**HDL Assembly**: A composition of connected HDL workers that are built into a complete FPGA
configuration bitstream, acting as an OpenCPI artifact. The resulting bitstream is executed on an
FPGA to implement some part or all of (the components of) the overall OpenCPI application.

For testing the Device/Subdevice Workers, an ``empty`` assembly is implemented. As the name
implies the assembly is void of Application Workers, therefore the Device/Subdevice Workers
must be instanced using a Container XML file.

The following is a description of the files needed in support of an ``empty`` assembly:

   * **hdl/assemblies/empty/empty.xml**: Is the OpenCPI Hardware Assembly Description (OHAD) file.
     It instances all of the Application Workers and their connections of the application or
     portion of the application. For the ``empty`` assembly, the special ``nothing`` worker is
     instance indicating that the assembly is actually void of any workers.

   * **hdl/assemblies/empty/cnt_z3u_i2c_bus2.xml**: Is the Container file.

      - **config, only, constraints**: Are top level attributes that provide the ability to declare
        which Platform Configuration, Platform, and constraints to build against, respectively.

      - **<device name='xyz'/>**: Instances a Device Worker(s) that are to be included. During the
        build process, the Subdevice Worker that supports the combination of all Device Workers
        listed in the Container file will be located and included in the build.

         - **Notice for the tmp103 Device Workers that an <index> is appended to each of its
           instances.**

   * **hdl/assemblies/empty/Makefile**: Supports restricting the building of the ``empty`` assembly
     to specific Containers via the **Containers=** make directive.

Device Proxy
^^^^^^^^^^^^

A **device proxy** is a software worker (RCC/C++) that is specifically paired with a device worker
in order to translate a higher level control interface for a class of devices into the lower level
actions required on a specific device.
`OpenCPI Platform Development Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf>`_

1. Understanding the proxy property elements.

The proxy property elements for any device proxy are highly flexible and can represent different
aspects of a given device. For instance, the tmp103 proxy property elements directly represent the
register set of the tmp103 component spec. However, for the sit5356 not all proxy property
elements directly represent the register set of the sit5356 component spec.

The proxy property elements of the tmp103 and sit5356 examples below will be included to help the
developer understand how these elements can be designed.

   * **tmp103 proxy property elements**:

      #. tmp103 [temperature] - Represents the human readable temperature data that is
         present on the temp_reg register.

      #. tmp103 [temp_high_threshold] - Checks that the user input temp_high_threshold value is
         within bounds of the tmp103 device threshold and sets the associated thigh_reg register.

   * **sit5356 proxy property elements**:

      #. sit5356 [ppm_shift_from_nominal] - A user input value that determines the value of the
         ctrl_word_value.

      #. sit5356 [ctrl_word_value] - Represents the value of a math equation defined in the sit5356
         datasheet which is implemented in the proxy. Once this value is determined it sets the
         dig_freq_ctrl_lsw_reg register and dig_freq_ctrl_msw_reg register of the sit5356 device.

      #. sit5356 [shifted_frequency] - Represents the 'expected' frequency shift of the sit5356
         device based on the how the dig_freq_ctrl_lsw_reg register and dig_freq_ctrl_msw_reg
         register are set. This value gives the user a software "expected" representation of a
         value that should be seen on hardware.

2. Define the Component Specification (OCS).

   * **specs/sit5356_proxy-spec.xml**: Is the proxy component spec for the sit5356. This proxy
     component spec defines the basic functions that will be used for the sit5356 and implements
     them as componentspec properties that will then manipulated in the sit5356 proxy.

      - **property name=**: Defines the componentspec property name that will then be used by
        the sit5356 proxy.

3. Select the component spec properties that should be incorporated into the design of the proxy
   and develop the functionality of the proxy.

   * **sit5356_proxy.rcc/sit5356_proxy.xml**: Is the OWD for the sit5356 Device Proxy.

      - **specproperty name=**: Defines the component spec property that will be implemented for
        the proxy.

   * **sit5356_proxy.cc**: Creates and defines the basic functionality of each of the properties.

Testing
-------

Application
^^^^^^^^^^^

An **application** is an assembly of configured and connected components. Each component in
the assembly indicates that, at runtime, some implementation - a worker binary - must
be found for this component and an executing runtime instance must be created from this binary.
Each component in the assembly can be assigned initial (startup) property values.
`OpenCPI User Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf>`_

The ``Testing`` stage of development encapsulates and tests the ``research`` and ``implementation``
phases of development. After the ``application`` has first been tested, the developer may
need to re-perform both the ``research``, and ``implementation`` phases again, until the
desired outcome is reached when running the test application.

#. Create a directory and an OpenCPI Application Specification (OAS) file

   * **applications/z3u_i2c_bus2_test/z3u_i2c_bus2_test.xml**: Is the OAS that instances the
     components to be tested. Optionally, properties can be instanced to further restrict the
     implementation search or for setting initial values upon execution of the application.

#. Setup embedded platform

   #. For you choice of Standalone, Network or Server Mode.

   #. Setup the OCPI_LIBRARY_PATH for locating run-time artifacts.

#. Application execution

   #. Execute the app for 1 sec and dump the property values.

      * ``ocpirun -v -d -x -t 1 z3u_i2c_bus2_test.xml``

   #. Review the post execution property values and compare them to the expected power-on reset
      values that are detailed in the device's datasheet register map.

      * The z3u-i2c-bus2-app.rst located in the applications/z3u_i2c_bus2_test/ directory contains
        an example of the standard output for Bus 2 of the Z3u.

   #. Confirm the ability to write/read device registers.

      - if a property is a ushort keep in mind `Issue 2663`

      * ``ocpihdl get -x <index> <property name>``
      * ``ocpihdl set -x <index> <property name> <value>``
      * ``ocpihdl get -x <index> <property name>``
