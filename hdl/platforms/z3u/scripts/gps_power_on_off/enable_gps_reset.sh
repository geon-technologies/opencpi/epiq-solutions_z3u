#!/bin/bash

# Active low reset
MIO_DIRECTION=$(devmem 0xFF0A0244)
MIO_OUTPUT_EN=$(devmem 0xFF0A0248)
if [[ "$MIO_DIRECTION" == "0x00000000" ]]; then
  devmem 0xFF0A0244 32 0x0000004C
fi

if [[ "$MIO_OUTPUT_EN" == "0x00000000" ]]; then
  devmem 0xFF0A0248 32 0x0000004C
fi

devmem 0xFF0A0008 32 0xFFFB0000
