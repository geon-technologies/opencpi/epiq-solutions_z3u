
architecture rtl of sit5356_worker is
begin
  -- Control plane outputs.  Raw props routed to underlying I2C
  rawprops_out.present         <= '1';
  rawprops_out.reset           <= ctl_in.reset;
  props_out.raw                <= rawprops_in.raw;
  --Need to shift to 16bit addressing
  rawprops_out.raw.address     <= props_in.raw.address srl 1;
  rawprops_out.raw.byte_enable <= props_in.raw.byte_enable;
  rawprops_out.raw.is_read     <= props_in.raw.is_read;
  rawprops_out.raw.is_write    <= props_in.raw.is_write;
  rawprops_out.raw.data        <= props_in.raw.data;
end rtl;
