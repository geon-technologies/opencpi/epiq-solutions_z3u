
#include "pcal6524_proxy-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Pcal6524_proxyWorkerTypes;

class Pcal6524_proxyWorker : public Pcal6524_proxyWorkerBase {
  RunCondition m_aRunCondition;
public:
  Pcal6524_proxyWorker() : m_aRunCondition(RCC_NO_PORTS) {
    //Run function should never be called
    setRunCondition(&m_aRunCondition);
  }
private:
  RCCResult run(bool /*timedout*/) {
    return RCC_DONE;
  }
};

PCAL6524_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
PCAL6524_PROXY_END_INFO
