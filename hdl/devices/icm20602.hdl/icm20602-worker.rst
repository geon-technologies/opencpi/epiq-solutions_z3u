.. icm20602 HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _icm20602-HDL-worker:

``icm20602`` HDL worker
=======================

Implementation of the icm20602 HDL worker for the TDK InvenSense icm-20602 accelerometer.

Detail
------

The icm20602 HDL worker uses the Raw Properties rawprops to interface with the
register map of the TDK InvenSense icm-20602 accelerometer.

References:

 * http://3cfeqx1hf82y3xcoull08ihx-wpengine.netdna-ssl.com/wp-content/uploads/2020/11/DS-000176-ICM-20602-v1.1.pdf

Control Timing and Signals
--------------------------

The icm20602 device worker operates entirely in the control plane clock domain. All I2C SCL and SDA
signals are generated in the control plane domain. Note that the I2C SCL can only be a divided
version of the control plane clock.
